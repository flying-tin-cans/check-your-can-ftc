#!/bin/sh
# Make sure script stops in error
set -e

# Copy the README.md into the flask folder, so the Dockerfile can copy it
cp README.md flask/README.md

# Start docker compose in detached mode
MYSQL_CYC_ROOT_PASSWORD="root-password" docker-compose up -d
