#!/bin/sh
flake8 ./flask --extend-exclude=dist,build,ftc-tools_map-env --show-source --statistics --ignore=E501,W504,E129
