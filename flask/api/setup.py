from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
import os


def create_app():
    create_dirs()

    # Create flask app
    # TODO add static css stuff
    app = Flask(__name__, template_folder="./../templates/")

    # SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead, so turn it off
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    # Add the MySQL database
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root-password@cyc-db/fates'

    # Secret key for sessions
    app.config['SECRET_KEY'] = 'secret-key-goes-here'  # TODO change secret key

    app.config['UPLOAD_FOLDER'] = "../uploads"
    # app.config['MAX_CONTENT_PATH']

    db.init_app(app)

    login_manager = LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    from models.user import User

    @login_manager.user_loader
    def load_user(user_id):
        # since the user_id is just the primary key of our user table, use it
        # in the query for the user
        return User.query.get(int(user_id))

    # Import all the routes
    from routes.index import index_blueprint
    from routes.map import map_blueprint
    from routes.process_mission import process_mission_blueprint
    from routes.fates import fates_blueprint
    from routes.manage_missions import manage_mission_blueprint
    from routes.auth import auth_blueprint
    from routes.profile import profile_blueprint
    from routes.upload_mission import upload_mission_blueprint
    from routes.pilots_not_linked import pilots_not_linked_blueprint
    from routes.readme import readme_blueprint
    from routes.admin import admin_blueprint
    from routes.delete_mission import delete_mission_blueprint
    from routes.wrap_up import wrap_up_blueprint
    from routes.recalc import recalc_blueprint

    # Register the different routes from the submodules
    app.register_blueprint(index_blueprint)
    app.register_blueprint(map_blueprint)
    app.register_blueprint(process_mission_blueprint)
    app.register_blueprint(fates_blueprint)
    app.register_blueprint(manage_mission_blueprint)
    app.register_blueprint(auth_blueprint)
    app.register_blueprint(profile_blueprint)
    app.register_blueprint(upload_mission_blueprint)
    app.register_blueprint(pilots_not_linked_blueprint)
    app.register_blueprint(readme_blueprint)
    app.register_blueprint(admin_blueprint)
    app.register_blueprint(delete_mission_blueprint)
    app.register_blueprint(wrap_up_blueprint)
    app.register_blueprint(recalc_blueprint)

    return app


def create_dirs():
    uploads_filepath = "../uploads"
    if not os.path.exists(uploads_filepath):
        os.makedirs(uploads_filepath)


# init SQLAlchemy so we can use it later in our models
db = SQLAlchemy()

app = create_app()
