from flask import Blueprint, send_file, request, render_template, redirect, url_for, flash
import folium
from folium.plugins import HeatMap
from flask_login import login_required, current_user
from json import loads

map_blueprint = Blueprint(name='map', import_name=__name__)

# Look these up from Il2-Mission planner "content.js"
lngMaxRheinland = 139.2
lngMaxKuban = 144.3
lngMaxNormandy = 160


@map_blueprint.route('/rheinland/tiles/<path:img>')
def get_image_rheinland(img):
    filename = f'../maps/rheinland/{img}'

    # Check if the tile exists, if not return default empty tile
    try:
        with open(filename):
            return send_file(filename, mimetype='image/png')
    except BaseException:
        return send_file("../maps/empty.png", mimetype='image/png')


@map_blueprint.route('/kuban/tiles/<path:img>')
def get_image_kuban(img):
    filename = f'../maps/kuban/{img}'

    # Check if the tile exists, if not return default empty tile
    try:
        with open(filename):
            return send_file(filename, mimetype='image/png')
    except BaseException:
        return send_file("../maps/empty.png", mimetype='image/png')


@map_blueprint.route('/normandy-early/tiles/<path:img>')
def get_image_normandy_early(img):
    filename = f'../maps/normandy-early/{img}'

    # Check if the tile exists, if not return default empty tile
    try:
        with open(filename):
            return send_file(filename, mimetype='image/png')
    except BaseException:
        return send_file("../maps/empty.png", mimetype='image/png')


@map_blueprint.route('/map', methods=['POST', 'GET'])
@login_required
def map_mission():
    if current_user.campaign_team is True:
        if request.method == 'POST':
            pam_mission_id = request.form.get('pam_mission_id')
            map_id = get_pam_map_id(pam_mission_id)

            m = get_map(map_id)

            add_frontlines(m, pam_mission_id, map_id)
            add_fates(m, pam_mission_id, True, None, map_id)

            folium.LayerControl().add_to(m)

            return m._repr_html_()
        else:
            return render_template("index.html")
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))


@map_blueprint.route('/map_fate')
def map_mission_pilot():
    pam_mission_id = request.args.get('pam_mission_id')
    pam_pilot_id = request.args.get('pam_pilot_id')
    map_id = get_pam_map_id(pam_mission_id)

    m = get_map(map_id)

    from models.fate import Fate
    fates = Fate.query.filter_by(
        mission_id=pam_mission_id).filter_by(
        pam_id=pam_pilot_id).all()

    if fates is not None:
        add_frontlines(m, pam_mission_id, map_id)
        add_fates(m, pam_mission_id, False, pam_pilot_id, map_id)
        folium.LayerControl().add_to(m)
        return m._repr_html_()
    else:
        return render_template("mission_not_processed.html")


def get_map(map_id):
    m = folium.Map(
        location=[-lngMaxRheinland / 2, lngMaxRheinland / 2],
        zoom_start=3,
        crs='Simple',
        tiles=None,
        attr='FTC\' check-your-fate'
    )

    if map_id == 3:
        attr = 'Kuban-Map'
        name = 'Kuban-Map'
        tiles = 'kuban/tiles/{z}/{x}/{y}.png'

    elif map_id == 6:
        attr = 'Rheinland-Map'
        name = 'Rheinland-Map'
        tiles = 'rheinland/tiles/{z}/{x}/{y}.png'

    elif map_id == 7:
        attr = 'Normandy-Map'
        name = 'Normandy-Map'
        tiles = 'normandy-early/tiles/{z}/{x}/{y}.png'

    # This should not happen
    else:
        attr = 'Empty-Map'
        name = 'Empty-Map'
        tiles = ''

    MapLayer = folium.raster_layers.TileLayer(
        attr=attr,
        name=name,
        tiles=tiles,
        control=True,
        min_zoom=-0,
        max_native_zoom=7,
        max_zoom=10,
        show=True,
        overlay=True)
    MapLayer.add_to(m)

    return m


def scaleDataPointX(X, map_id):
    if map_id == 6:
        if X is not None:
            Xnew = X - 30000
            Xnew = float((Xnew * lngMaxRheinland) / (430800 - 30000))
            Xnew = conversePointInImage(Xnew, lngMaxRheinland)
            Xnew = -1 * Xnew
            return Xnew
        else:
            pass
    elif map_id == 3:
        if X is not None:
            Xnew = X - 35200
            Xnew = float((Xnew * lngMaxKuban) / (450930 - 35200))
            Xnew = conversePointInImage(Xnew, lngMaxKuban)
            Xnew = -1 * Xnew
            return Xnew
        else:
            pass
    elif map_id == 7:
        if X is not None:
            Xnew = X
            Xnew = float((Xnew * lngMaxNormandy) / (396800))
            Xnew = conversePointInImage(Xnew, lngMaxNormandy)
            Xnew = -1 * Xnew
            return Xnew
        else:
            pass
    else:
        pass


def scaleDataPointZ(Z, map_id):
    if map_id == 6:
        if Z is not None:
            Znew = Z - 30000
            Znew = float((Znew * lngMaxRheinland) / (430800 - 30000))
            return Znew
        else:
            pass
    elif map_id == 3:
        # TODO change for Kuban?
        if Z is not None:
            Znew = Z - 35200
            Znew = float((Znew * lngMaxKuban) / (450930 - 35200))
            return Znew
        else:
            pass
    elif map_id == 7:
        if Z is not None:
            Znew = Z
            Znew = float((Znew * lngMaxNormandy) / (396800))
            return Znew
        else:
            pass
    else:
        pass


def conversePointInImage(point, maxOnThatAxis):
    diffToMin = point
    diffToMax = maxOnThatAxis - point

    if diffToMin > diffToMax:
        newPoint = maxOnThatAxis - diffToMin
    else:
        newPoint = diffToMax

    return newPoint


def add_fates(m, pam_mission_id, overview, pam_pilot_id, map_id):
    feature_group_heatMap = folium.map.FeatureGroup(name="Heat Map")
    # feature_group_takeoff = folium.map.FeatureGroup(name="Take-Off")
    feature_group_landed = folium.map.FeatureGroup(name="Landed")
    feature_group_bailed = folium.map.FeatureGroup(name="Bailed")
    feature_group_crashed = folium.map.FeatureGroup(name="Crashed")
    feature_group_ditched = folium.map.FeatureGroup(name="Ditched")
    feature_pow = folium.map.FeatureGroup(name="POW")
    feature_escaped = folium.map.FeatureGroup(name="Escpaed")
    feature_group_died = folium.map.FeatureGroup(name="Died")

    heatMapPoints = []

    from models.fate import Fate
    from models.mission import Mission
    from calculations.fate import Fate as FateEnum

    # TODO: what happens if we have a pam-mission which needs 2 mission ids,
    # like after a server restart?
    mission = Mission.query.filter_by(pam_id=pam_mission_id).first()

    mission_id = int(mission.id)

    if overview is True:
        fates = Fate.query.filter_by(mission_id=mission_id).all()
    else:
        fates = Fate.query.filter_by(
            mission_id=mission_id).filter_by(
            pam_id=pam_pilot_id).all()

    for fate in fates:
        tooltip = fate.name

        popup = folium.Popup(
            fate.name +
            "<br>" +
            fate.fate.toString(
                fate.fate) +
            "<br>" +
            "Pilot status: " +
            fate.health +
            "<br>" +
            "Aircraft status: " +
            fate.aircraft_status)

        # takeoff_position_x_scaled = scaleDataPointX(fate.takeoff_position_x)
        # takeoff_position_z_scaled = scaleDataPointZ(fate.takeoff_position_z)
        landed_position_x_scaled = scaleDataPointX(
            fate.landed_position_x, map_id)
        landed_position_z_scaled = scaleDataPointZ(
            fate.landed_position_z, map_id)
        ditched_position_x_scaled = scaleDataPointX(
            fate.ditched_position_x, map_id)
        ditched_position_z_scaled = scaleDataPointZ(
            fate.ditched_position_z, map_id)
        crashed_position_x_scaled = scaleDataPointX(
            fate.crashed_position_x, map_id)
        crashed_position_z_scaled = scaleDataPointZ(
            fate.crashed_position_z, map_id)
        bailout_position_x_scaled = scaleDataPointX(
            fate.bailout_position_x, map_id)
        bailout_position_z_scaled = scaleDataPointZ(
            fate.bailout_position_z, map_id)
        target_position_x_scaled = scaleDataPointX(
            fate.target_position_x, map_id)
        target_position_z_scaled = scaleDataPointZ(
            fate.target_position_z, map_id)
        captured_position_x_scaled = scaleDataPointX(
            fate.captured_position_x, map_id)
        captured_position_z_scaled = scaleDataPointZ(
            fate.captured_position_z, map_id)
        dead_position_x_scaled = scaleDataPointX(fate.dead_position_x, map_id)
        dead_position_z_scaled = scaleDataPointZ(fate.dead_position_z, map_id)
        start_position_x_scaled = scaleDataPointX(
            fate.start_position_x, map_id)
        start_position_z_scaled = scaleDataPointZ(
            fate.start_position_z, map_id)

        # TODO: Debug the take-off position
        # position_takeoff = [takeoff_position_x_scaled,
        #            takeoff_position_z_scaled]
        # heatMapPoints.append(position_takeoff)
        # icon = folium.Icon(color='green')
        # take_off_marker = folium.Marker(
        #    position_takeoff, popup=popup, tooltip=tooltip, icon=icon)
        # feature_group_takeoff.add_child(take_off_marker)

        if fate.fate == FateEnum.NOT_CALCULATED:
            from calculations.utils import Utils
            utils = Utils(False, False, False)
            utils.errorPrint(
                "That should not happen! Following data caused this:")
            utils.errorPrint(fate)

        elif fate.fate == FateEnum.DISCONNECTED:
            pass

        elif fate.fate == FateEnum.LANDED:
            position = [landed_position_x_scaled,
                        landed_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='green')
            landed_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_landed.add_child(landed_marker)

        elif fate.fate == FateEnum.BAILED:
            position = [bailout_position_x_scaled,
                        bailout_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='blue')
            bailed_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_bailed.add_child(bailed_marker)

        elif fate.fate == FateEnum.CRASHED:
            position = [crashed_position_x_scaled,
                        crashed_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='blue')
            crashed_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_crashed.add_child(crashed_marker)

        elif fate.fate == FateEnum.DITCHED:
            position = [ditched_position_x_scaled,
                        ditched_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='blue')
            ditched_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_ditched.add_child(ditched_marker)

        elif fate.fate == FateEnum.POW:
            # Draw the walk home line
            positions = []
            positions.append(
                [start_position_x_scaled, start_position_z_scaled])
            positions.append(
                [target_position_x_scaled, target_position_z_scaled])
            way_home_line = folium.PolyLine(
                positions,
                popup="Way home",
                color="black",
                weight=2.5,
                opacity=1)
            feature_pow.add_child(way_home_line)

            # Draw the start point
            position = [start_position_x_scaled, start_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='blue')
            start_point_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_pow.add_child(start_point_marker)

            # Draw the captured point
            position1 = [captured_position_x_scaled,
                         captured_position_z_scaled]
            captured_circle = folium.Circle(
                position1,
                radius=0.2,
                popup="Point of capture",
                color="black",
                fill=False)
            feature_pow.add_child(captured_circle)

        elif fate.fate == FateEnum.ESCAPED:
            # Draw the walk home line
            positions = []
            positions.append(
                [start_position_x_scaled, start_position_z_scaled])
            positions.append(
                [target_position_x_scaled, target_position_z_scaled])
            way_home_line = folium.PolyLine(
                positions,
                popup="Way home",
                color="black",
                weight=2.5,
                opacity=1)
            feature_escaped.add_child(way_home_line)

            # Draw the start point
            position = [start_position_x_scaled, start_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='blue')
            start_point_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_escaped.add_child(start_point_marker)

        elif fate.fate == FateEnum.DEAD_FRIENDLY_TERRITORY:
            position = [dead_position_x_scaled,
                        dead_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='red')
            dead_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_died.add_child(dead_marker)

        elif fate.fate == FateEnum.DEAD_ENEMY_TERRITORY:
            position = [dead_position_x_scaled,
                        dead_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='red')
            dead_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_died.add_child(dead_marker)

        elif fate.fate == FateEnum.DEAD_NEUTRAL_TERRITORY:
            position = [dead_position_x_scaled,
                        dead_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='red')
            dead_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_died.add_child(dead_marker)

        elif fate.fate == FateEnum.DROWNED:
            position = [dead_position_x_scaled,
                        dead_position_z_scaled]
            heatMapPoints.append(position)
            icon = folium.Icon(color='red')
            dead_marker = folium.Marker(
                position, popup=popup, tooltip=tooltip, icon=icon)
            feature_group_died.add_child(dead_marker)

        elif fate.fate == FateEnum.RESCUED_FROM_FRIENDLY_FORCES:
            if fate.fate_detail == FateEnum.BAILED:
                position = [bailout_position_x_scaled,
                            bailout_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                bailed_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_bailed.add_child(bailed_marker)
            elif fate.fate_detail == FateEnum.CRASHED:
                position = [crashed_position_x_scaled,
                            crashed_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                crashed_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_crashed.add_child(crashed_marker)
            elif fate.fate_detail == FateEnum.DITCHED:
                position = [ditched_position_x_scaled,
                            ditched_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                ditched_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_ditched.add_child(ditched_marker)

        elif fate.fate == FateEnum.RESCUED_FROM_ENEMY_FORCES:
            if fate.fate_detail == FateEnum.BAILED:
                position = [bailout_position_x_scaled,
                            bailout_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                bailed_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_bailed.add_child(bailed_marker)
            elif fate.fate_detail == FateEnum.CRASHED:
                position = [crashed_position_x_scaled,
                            crashed_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                crashed_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_crashed.add_child(crashed_marker)
            elif fate.fate_detail == FateEnum.DITCHED:
                position = [ditched_position_x_scaled,
                            ditched_position_z_scaled]
                heatMapPoints.append(position)
                icon = folium.Icon(color='blue')
                ditched_marker = folium.Marker(
                    position, popup=popup, tooltip=tooltip, icon=icon)
                feature_group_ditched.add_child(ditched_marker)

        else:
            from calculations.utils import Utils
            utils = Utils(False, False, False)
            utils.errorPrint(
                "That should not happen! Following data caused this:")
            utils.errorPrint(fate)

    if overview is True:
        heatMap = HeatMap(heatMapPoints)
        feature_group_heatMap.add_child(heatMap)
        m.add_child(feature_group_heatMap)
    else:
        # no heatmap
        pass

    # m.add_child(feature_group_takeoff)
    m.add_child(feature_group_landed)
    m.add_child(feature_group_bailed)
    m.add_child(feature_group_crashed)
    m.add_child(feature_group_ditched)
    m.add_child(feature_pow)
    m.add_child(feature_escaped)
    m.add_child(feature_group_died)


def add_frontlines(m, pam_mission_id, map_id):
    feature_group_frontline_red = folium.map.FeatureGroup(name="Frontline Red")
    feature_group_frontline_blue = folium.map.FeatureGroup(
        name="Frontline Blue")

    from models.mission import Mission
    mission = Mission.query.filter_by(pam_id=pam_mission_id).first()

    frontline_red_string = mission.frontline_red
    frontline_blue_string = mission.frontline_blue

    frontline_red_string = frontline_red_string.replace("\'", "\"")
    frontline_blue_string = frontline_blue_string.replace("\'", "\"")

    frontline_red = loads(frontline_red_string)
    frontline_blue = loads(frontline_blue_string)

    positions_frontline_red = []
    positions_frontline_blue = []

    for coordinate_red in frontline_red:
        positions_frontline_red.append(
            [
                scaleDataPointX(
                    float(
                        coordinate_red["XPos"]), map_id), scaleDataPointZ(
                    float(
                        coordinate_red["ZPos"]), map_id)])

    for coordinate_blue in frontline_blue:
        positions_frontline_blue.append(
            [
                scaleDataPointX(
                    float(
                        coordinate_blue["XPos"]), map_id), scaleDataPointZ(
                    float(
                        coordinate_blue["ZPos"]), map_id)])

    frontline_red_line = folium.PolyLine(
        positions_frontline_red, color="red", weight=2.5, opacity=1)
    frontline_blue_line = folium.PolyLine(
        positions_frontline_blue, color="blue", weight=2.5, opacity=1)

    feature_group_frontline_red.add_child(frontline_red_line)
    feature_group_frontline_blue.add_child(frontline_blue_line)

    m.add_child(feature_group_frontline_red)
    m.add_child(feature_group_frontline_blue)


def get_pam_map_id(pam_mission_id):
    from calculations.pam import Pam
    from calculations.utils import Utils
    from json import load

    utils = Utils(False, False, False)

    try:
        with open("../configs/secret.json", "r") as json_file:
            secrets = load(json_file)

        PAM_user = secrets["PAM_user"]
        PAM_password = secrets["PAM_password"]
        PAM_host = secrets["PAM_host"]
        PAM_database = secrets["PAM_database"]

    except FileNotFoundError as err:

        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    pam = Pam(PAM_host, PAM_user, PAM_password, PAM_database, utils)
    pam.createConnection()

    # Get the map id which the campaign is using
    query = (
        "SELECT pam.campaign.platform AS map_id FROM pam.campaign RIGHT JOIN pam.mission ON pam.campaign.id = pam.mission.campaign_id WHERE pam.mission.id = " +
        pam_mission_id +
        ";")
    query_return = pam.query(query)

    # This is kind of an annoying hack:
    # The list will just contain one element, which is a tuple
    # and only the first element of the tuple is our map id
    pam_map_id_list = []
    for (map_id) in query_return:
        pam_map_id_list.append(map_id)

    pam_map_id = pam_map_id_list[0]

    pam.closeConnection()

    return pam_map_id[0]
