from flask import Blueprint, render_template, redirect, url_for, flash
from json import load
from flask_login import login_required, current_user

pilots_not_linked_blueprint = Blueprint(
    name='pilots_not_linked', import_name=__name__)


@pilots_not_linked_blueprint.route("/pilots_not_linked")
@login_required
def fates():
    if current_user.staff is True:
        pam_pilots_and_box_id = get_pam_pilots_and_box_id()
        il2stats_pilots_and_box_id = get_il2stats_pilots_and_box_id()

        pilots_not_linked = get_pilots_not_linked(
            pam_pilots_and_box_id, il2stats_pilots_and_box_id)

        return render_template(
            'pilots_not_linked.html',
            pilots_not_linked=pilots_not_linked)
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))


def get_pam_pilots_and_box_id():
    from calculations.pam import Pam
    from calculations.utils import Utils

    utils = Utils(False, False, False)

    try:
        with open("../configs/secret.json", "r") as json_file:
            secrets = load(json_file)

        PAM_user = secrets["PAM_user"]
        PAM_password = secrets["PAM_password"]
        PAM_host = secrets["PAM_host"]
        PAM_database = secrets["PAM_database"]

    except FileNotFoundError as err:

        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    pam = Pam(PAM_host, PAM_user, PAM_password, PAM_database, utils)
    pam.createConnection()

    query = (
        "SELECT id AS pam_id, callsign AS pam_callsign, boxid AS pam_boxid FROM member;")
    query_return = pam.query(query)

    pam_pilots_and_box_id = []
    for (pam_id, pam_callsign, pam_boxid) in query_return:
        pam_pilots_and_box_id.append({"pam_id": pam_id,
                                      "pam_callsign": pam_callsign,
                                      "pam_boxid": pam_boxid,
                                      "is_in_il2stats": False})

    pam.closeConnection()

    return pam_pilots_and_box_id


def get_il2stats_pilots_and_box_id():
    from calculations.il2_stats import Il2_stats
    from calculations.utils import Utils

    utils = Utils(False, False, False)

    try:
        with open("../configs/secret.json", "r") as json_file:
            secrets = load(json_file)

        IL2stats_user = secrets["IL2stats_user"]
        IL2stats_password = secrets["IL2stats_password"]
        IL2stats_host = secrets["IL2stats_host"]
        IL2stats_database = secrets["IL2stats_database"]

    except FileNotFoundError as err:

        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    il2_stats = Il2_stats(
        IL2stats_host,
        IL2stats_user,
        IL2stats_password,
        IL2stats_database,
        utils)
    il2_stats.createConnection()

    query = (
        "SELECT nickname AS il2stats_nickname, uuid AS il2stats_boxid FROM profiles;")
    query_return = il2_stats.query(query)

    il2stats_pilots_and_box_id = []
    for (il2stats_nickname, il2stats_boxid) in query_return:
        il2stats_pilots_and_box_id.append(
            {"il2stats_nickname": il2stats_nickname, "il2stats_boxid": il2stats_boxid, "is_in_pam": False})

    il2_stats.closeConnection()

    return il2stats_pilots_and_box_id


def get_pilots_not_linked(pilot_list_pam, pilot_list_il2stats):
    for pam_pilot in pilot_list_pam:
        for pilot_il2stats in pilot_list_il2stats:
            if pam_pilot["pam_boxid"] == pilot_il2stats["il2stats_boxid"]:
                pam_pilot["is_in_il2stats"] = True
                pilot_il2stats["is_in_pam"] = True

    pilots_not_linked = []

    for pam_pilot in pilot_list_pam:
        if pam_pilot["is_in_il2stats"] is False:
            pilots_not_linked.append(pam_pilot)

    for pilot_il2stats in pilot_list_il2stats:
        if pilot_il2stats["is_in_pam"] is False:
            pilots_not_linked.append(pilot_il2stats)

    return pilots_not_linked
