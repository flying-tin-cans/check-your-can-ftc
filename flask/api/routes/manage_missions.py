from flask import Blueprint, render_template, request, redirect, url_for, flash
from os import listdir
from flask_login import login_required, current_user

manage_mission_blueprint = Blueprint(
    name='manage_mission', import_name=__name__)


@manage_mission_blueprint.route('/manage_mission', methods=['POST', 'GET'])
@login_required
def manage_mission():
    if current_user.campaign_team is True:
        from models.mission import Mission
        from setup import db
        from json import loads

        if request.method == 'POST':
            # PAM informations
            # NOTE: request.form.get returns a string instead of a dictionary,
            # has to be converted first
            pam_missions_on_debriefing = request.form.get(
                'pam_missions_on_debriefing')
            pam_missions_on_debriefing = pam_missions_on_debriefing.replace(
                "'", "\"")
            pam_missions_on_debriefing = loads(pam_missions_on_debriefing)
            # TODO remove the campaign id, mission_id should be enough
            pam_campaign_id = pam_missions_on_debriefing['pam_campaign_id']
            pam_id = pam_missions_on_debriefing['pam_id']
            # TODO remove the name, mission_id should be enough
            pam_name = pam_missions_on_debriefing['pam_name']

            # IL2stats informations
            il2stats_missions_not_linked = request.form.get(
                'il2stats_missions_not_linked')
            il2stats_missions_not_linked = il2stats_missions_not_linked.replace(
                "'", "\"")
            il2stats_missions_not_linked = loads(il2stats_missions_not_linked)
            il2stats_id = il2stats_missions_not_linked['il2stats_id']

            # Mission-file
            filename = request.form.get('missionfiles')

            processed = False

            new_mission = Mission(pam_campaign_id=pam_campaign_id,
                                  pam_id=pam_id,
                                  pam_name=pam_name,
                                  il2stats_id=il2stats_id,
                                  filename=filename,
                                  processed=processed)

            try:
                db.session.add(new_mission)
                db.session.commit()
            except BaseException:
                return 'There was an issue adding the new mission'

        return render_template(
            'missions.html',
            missions=Mission.query.all(),
            missionfiles=get_uploaded_missions(),
            pam_missions_on_debriefing=get_pam_missions_on_debriefing(),
            il2stats_missions_not_linked=get_il2stats_missions_not_linked())
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))


def get_uploaded_missions():
    missionfiles = listdir("../uploads")

    return missionfiles


def get_pam_missions_on_debriefing():
    from calculations.pam import Pam
    from calculations.utils import Utils
    from json import load

    utils = Utils(False, False, False)

    try:
        with open("../configs/secret.json", "r") as json_file:
            secrets = load(json_file)

        PAM_user = secrets["PAM_user"]
        PAM_password = secrets["PAM_password"]
        PAM_host = secrets["PAM_host"]
        PAM_database = secrets["PAM_database"]

    except FileNotFoundError as err:

        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    pam = Pam(PAM_host, PAM_user, PAM_password, PAM_database, utils)
    pam.createConnection()

    # Get only the missions which are on "Debriefing", because we do not need
    # the others
    query = ("SELECT id as pam_id, campaign_id as pam_campaign_id, name as pam_name FROM campaign_mission_info WHERE mission_status = 1;")
    query_return = pam.query(query)

    pam_missions_on_debriefing = []
    for (pam_id, pam_campaign_id, pam_name) in query_return:
        pam_missions_on_debriefing.append(
            {"pam_id": pam_id, "pam_campaign_id": pam_campaign_id, "pam_name": pam_name})

    pam.closeConnection()

    return pam_missions_on_debriefing


def get_il2stats_missions_not_linked():
    from calculations.il2_stats import Il2_stats
    from calculations.utils import Utils
    from json import load
    from models.mission import Mission

    utils = Utils(False, False, False)

    try:
        with open("../configs/secret.json", "r") as json_file:
            secrets = load(json_file)

        IL2stats_user = secrets["IL2stats_user"]
        IL2stats_password = secrets["IL2stats_password"]
        IL2stats_host = secrets["IL2stats_host"]
        IL2stats_database = secrets["IL2stats_database"]

    except FileNotFoundError as err:

        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    il2_stats = Il2_stats(
        IL2stats_host,
        IL2stats_user,
        IL2stats_password,
        IL2stats_database,
        utils)
    il2_stats.createConnection()

    query = ("SELECT id AS il2stats_id, NAME AS il2stats_name FROM missions WHERE is_hide=false;")
    query_return = il2_stats.query(query)

    il2stats_missions = []
    for (il2stats_id, il2stats_name) in query_return:
        il2stats_missions.append(
            {"il2stats_id": il2stats_id, "il2stats_name": il2stats_name})

    il2_stats.closeConnection()

    # Filter out the already linked ones
    missions = Mission.query.all()

    il2stats_missions_not_linked = []
    if missions is not None:
        for il2stats_mission in il2stats_missions:
            flag = False
            for mission in missions:
                if mission.il2stats_id == il2stats_mission["il2stats_id"]:
                    flag = True
            if flag is True:
                pass
            else:
                il2stats_missions_not_linked.append(il2stats_mission)

    return il2stats_missions_not_linked
