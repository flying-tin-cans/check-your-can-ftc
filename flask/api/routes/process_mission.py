from flask import Blueprint, render_template, redirect, url_for, flash
from calculations.calc_your_can import Calc_your_can
from flask_login import login_required, current_user
from os import remove

process_mission_blueprint = Blueprint(
    name='process_mission', import_name=__name__)


@process_mission_blueprint.route('/process_mission')
@login_required
def process_mission():
    if current_user.campaign_team is True:
        from models.mission import Mission
        from models.fate import Fate
        from calculations.fate import Fate as FateEnum
        from setup import db
        from models.wrap_up import Wrap_Up

        missions = Mission.query.all()

        for mission in missions:
            if (mission.processed is False) and (mission.il2stats_id != ''):
                calc_your_can = Calc_your_can("../configs/secret.json")
                mission_path = "../uploads/" + mission.filename
                fates, wrap_ups, frontline = calc_your_can.executeForMission(
                    mission.il2stats_id, mission.pam_id, mission_path, "../configs/probabilities.json",
                    "../maps")

                for fate in fates:
                    takeoff_position_x = None
                    takeoff_position_z = None
                    landed_position_x = None
                    landed_position_z = None
                    ditched_position_x = None
                    ditched_position_z = None
                    bailout_position_x = None
                    bailout_position_z = None
                    target_position_x = None
                    target_position_z = None
                    captured_position_x = None
                    captured_position_z = None
                    dead_position_x = None
                    dead_position_z = None
                    start_position_x = None
                    start_position_z = None
                    crashed_position_x = None
                    crashed_position_z = None

                    if fate["takeoff_position"] is not None:
                        takeoff_position_x = fate["takeoff_position"]["XPos"]
                        takeoff_position_z = fate["takeoff_position"]["ZPos"]
                    if fate["landed_position"] is not None:
                        landed_position_x = fate["landed_position"]["XPos"]
                        landed_position_z = fate["landed_position"]["ZPos"]
                    if fate["ditched_position"] is not None:
                        ditched_position_x = fate["ditched_position"]["XPos"]
                        ditched_position_z = fate["ditched_position"]["ZPos"]
                    if fate["bailout_position"] is not None:
                        bailout_position_x = fate["bailout_position"]["XPos"]
                        bailout_position_z = fate["bailout_position"]["ZPos"]
                    if fate["target_position"] is not None:
                        target_position_x = fate["target_position"]["XPos"]
                        target_position_z = fate["target_position"]["ZPos"]
                    if fate["captured_position"] is not None:
                        captured_position_x = fate["captured_position"]["XPos"]
                        captured_position_z = fate["captured_position"]["ZPos"]
                    if fate["dead_position"] is not None:
                        dead_position_x = fate["dead_position"]["XPos"]
                        dead_position_z = fate["dead_position"]["ZPos"]
                    if fate["start_position"] is not None:
                        start_position_x = fate["start_position"]["XPos"]
                        start_position_z = fate["start_position"]["ZPos"]
                    if fate["crashed_position"] is not None:
                        crashed_position_x = fate["crashed_position"]["XPos"]
                        crashed_position_z = fate["crashed_position"]["ZPos"]

                    if fate["fate"] == FateEnum.DROWNED:
                        if fate["fate_detail"] == FateEnum.CRASHED:
                            dead_position_x = crashed_position_x
                            dead_position_z = crashed_position_z
                        elif fate["fate_detail"] == FateEnum.DITCHED:
                            # Annoying behavior: ditched as fate, but only bailed position is smth that can happen.
                            if (ditched_position_x is None) or (ditched_position_z is None):
                                dead_position_x = bailout_position_x
                                dead_position_z = bailout_position_z
                            else:
                                dead_position_x = ditched_position_x
                                dead_position_z = ditched_position_z
                        elif fate["fate_detail"] == FateEnum.BAILED:
                            dead_position_x = bailout_position_x
                            dead_position_z = bailout_position_z

                    if fate["fate_detail"] == FateEnum.DITCHED:
                        # Annoying behavior: ditched as fate, but only bailed position is smth that can happen.
                        if (ditched_position_x is None) or (ditched_position_z is None):
                            ditched_position_x = bailout_position_x
                            ditched_position_z = bailout_position_z

                    new_fate = Fate(
                        mission_id=mission.id,
                        name=fate["pilot_name"],
                        pam_id=fate["pam_id"],
                        unit=fate["pilot_unit"],
                        fate=fate["fate"],
                        fate_detail=fate["fate_detail"],
                        health=fate["health"],
                        wound=fate["wound"],
                        aircraft_status=fate["aircraft_status"],
                        damage=fate["damage"],
                        takeoff_position_x=takeoff_position_x,
                        takeoff_position_z=takeoff_position_z,
                        landed_position_x=landed_position_x,
                        landed_position_z=landed_position_z,
                        ditched_position_x=ditched_position_x,
                        ditched_position_z=ditched_position_z,
                        bailout_position_x=bailout_position_x,
                        bailout_position_z=bailout_position_z,
                        target_position_x=target_position_x,
                        target_position_z=target_position_z,
                        captured_position_x=captured_position_x,
                        captured_position_z=captured_position_z,
                        dead_position_x=dead_position_x,
                        dead_position_z=dead_position_z,
                        start_position_x=start_position_x,
                        start_position_z=start_position_z,
                        crashed_position_x=crashed_position_x,
                        crashed_position_z=crashed_position_z

                    )

                    db.session.add(new_fate)
                    db.session.commit()

                    try:
                        db.session.add(new_fate)
                        db.session.commit()

                    except BaseException:
                        return 'There was an issue adding the new fate'

                for wrap_up in wrap_ups:
                    new_wrap_up = Wrap_Up(
                        mission_id=mission.id,
                        base_unit_name=wrap_up["baseUnitName"],
                        sorties=wrap_up["sorties"],
                        pilotHealthy=wrap_up["pilotHealthy"],
                        pilotWounded=wrap_up["pilotWounded"],
                        pilotDead=wrap_up["pilotDead"],
                        aircraftUnharmed=wrap_up["aircraftUnharmed"],
                        aircraftDamaged=wrap_up["aircraftDamaged"],
                        aircraftDestroyed=wrap_up["aircraftDestroyed"],
                        pilotLossPercentage=wrap_up["pilotLossPercentage"],
                        aircraftLossPercentage=wrap_up["aircraftLossPercentage"])

                    try:
                        db.session.add(new_wrap_up)
                        db.session.commit()

                    except BaseException:
                        return 'There was an issue adding the new wrap_up'

                mission.frontline_red = str(frontline["red"])
                mission.frontline_blue = str(frontline["blue"])

                mission.processed = True
                db.session.add(mission)
                db.session.commit()

                # Delete the mission file from the server after succesfull
                # process
                try:
                    remove(mission_path)
                except FileNotFoundError:
                    # Already deleted, can happen if process mission was called 2 times through a double click
                    # so nothing of concern
                    None

            else:
                pass

        from routes.manage_missions import get_uploaded_missions, get_pam_missions_on_debriefing, get_il2stats_missions_not_linked

        return render_template(
            'missions.html',
            missions=Mission.query.all(),
            missionfiles=get_uploaded_missions(),
            pam_missions_on_debriefing=get_pam_missions_on_debriefing(),
            il2stats_missions_not_linked=get_il2stats_missions_not_linked())
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
