from flask import Blueprint, redirect, url_for, flash, request
from flask_login import login_required, current_user
from calculations.calc_your_can import Calc_your_can

recalc_blueprint = Blueprint(name='recalc', import_name=__name__)


@recalc_blueprint.route("/recalc", methods=['POST', 'GET'])
@login_required
def recalc():
    if (current_user.staff is True) or (current_user.campaign_team is True):
        # TODO add again
        # from models.fate import Fate

        fate_id = request.form.get('fate_id')
        fate_id = fate_id.replace("'", "\"")

        # TODO add again
        # fate = Fate.query.filter_by(id=fate_id).all()

        calc_your_can = Calc_your_can("../configs/secret.json")
        calc_your_can.recalc(fate_id)

        flash('Recalculated fate.')
        return redirect(url_for('index.index'))
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
