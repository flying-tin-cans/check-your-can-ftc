from flask import Blueprint, render_template
from flask_login import login_required, current_user

profile_blueprint = Blueprint('profile', __name__)


@profile_blueprint.route('/profile')
@login_required
def profile():
    return render_template(
        'profile.html',
        name=current_user.name,
        admin=current_user.admin,
        campaign_team=current_user.campaign_team,
        staff=current_user.staff)
