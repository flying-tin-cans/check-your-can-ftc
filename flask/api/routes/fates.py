from flask import Blueprint, render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user

fates_blueprint = Blueprint(name='fates', import_name=__name__)


@fates_blueprint.route("/fates", methods=['POST', 'GET'])
@login_required
def fates():
    if (current_user.staff is True) or (current_user.campaign_team is True):
        from models.fate import Fate
        from models.mission import Mission
        from routes.index import get_processed_missions

        mission_id = request.form.get('mission_id')
        fates = Fate.query.filter_by(
            mission_id=mission_id).order_by(
            Fate.unit).order_by(
            Fate.name).all()

        mission = Mission.query.filter_by(id=mission_id).first()

        if request.method == "POST":
            flash("Fates for mission: " + mission.pam_name)

        return render_template('fates.html',
                               processed_missions=get_processed_missions(),
                               fates=fates)
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
