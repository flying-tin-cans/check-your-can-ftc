from flask import Blueprint
from flask_login import login_required
from markdown import markdown

readme_blueprint = Blueprint('readme', __name__)


@readme_blueprint.route('/readme')
@login_required
def readme():
    try:
        with open("../README.md", "r") as readme_file:
            readme_string = markdown(readme_file.read())

    except FileNotFoundError as err:
        from calculations.utils import Utils
        utils = Utils(False, False, False)
        utils.errorPrint(err)
        utils.fatalError("Config read-out failed")

    return readme_string
