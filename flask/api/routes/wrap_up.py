from flask import Blueprint, render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user


wrap_up_blueprint = Blueprint(name='wrap_up', import_name=__name__)


@wrap_up_blueprint.route('/wrap_up', methods=['POST', 'GET'])
@login_required
def wrap_up():
    if current_user.campaign_team is True:
        from models.wrap_up import Wrap_Up
        from models.mission import Mission
        from routes.index import get_processed_missions

        mission_id = request.form.get('mission_id')
        wrap_ups = Wrap_Up.query.filter_by(mission_id=mission_id).all()

        mission = Mission.query.filter_by(id=mission_id).first()

        if request.method == "POST":
            flash("Wrap up for mission: " + mission.pam_name)

        return render_template(
            'wrap_up.html',
            processed_missions=get_processed_missions(),
            wrap_ups=wrap_ups)
    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
