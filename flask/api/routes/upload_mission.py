from flask import Blueprint, render_template, flash, request, redirect, url_for
from werkzeug.utils import secure_filename
from flask_login import login_required, current_user
import os

upload_mission_blueprint = Blueprint(
    name='upload_mission', import_name=__name__)

ALLOWED_EXTENSIONS = {'mission'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@upload_mission_blueprint.route("/upload_mission", methods=["GET", "POST"])
@login_required
def upload_mission():
    if current_user.campaign_team is True:
        if request.method == "POST":
            # check if the post request has the file part
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']

            # If the user does not select a file, the browser submits an
            # empty file without a filename.
            if file.filename == '':
                flash('No selected file')
                return redirect(request.url)

            if file and allowed_file(file.filename):
                from setup import app
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                flash('Uploaded', filename)
                return redirect(request.url)
            else:
                flash('Filename not allowed, has to be *.mission file')
                return redirect(request.url)

        else:
            from models.mission import Mission
            from routes.manage_missions import get_uploaded_missions, get_pam_missions_on_debriefing, get_il2stats_missions_not_linked

            return render_template(
                'missions.html',
                missions=Mission.query.all(),
                missionfiles=get_uploaded_missions(),
                pam_missions_on_debriefing=get_pam_missions_on_debriefing(),
                il2stats_missions_not_linked=get_il2stats_missions_not_linked())

    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
