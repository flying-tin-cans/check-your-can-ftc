from flask import Blueprint, render_template, redirect, url_for, flash, request
from flask_login import login_required, current_user

delete_mission_blueprint = Blueprint(
    name='delete_mission', import_name=__name__)


@delete_mission_blueprint.route('/delete_mission', methods=['POST', 'GET'])
@login_required
def delete_mission():
    if current_user.admin is True:
        if request.method == "POST":
            # Get the ID from the mission we want to delete
            mission_id = request.form.get("mission_id")

            if mission_id is None:
                flash('ERROR: mission_id was None')
                return redirect(request.url)
            else:
                # First we have to delete the fates of the corresponding
                # mission:
                from models.fate import Fate

                fates_of_mission = Fate.query.filter_by(
                    mission_id=mission_id).all()

                from setup import db

                try:
                    for fate_in_mission in fates_of_mission:
                        db.session.delete(fate_in_mission)

                    db.session.commit()
                except BaseException:
                    return 'There was an issue deleting the fates of the mission'

                # Second we have to delete the mission wrap-up
                from models.wrap_up import Wrap_Up

                wrap_ups_of_mission = Wrap_Up.query.filter_by(
                    mission_id=mission_id).all()

                try:
                    for wrap_up_of_mission in wrap_ups_of_mission:
                        db.session.delete(wrap_up_of_mission)

                    db.session.commit()
                except BaseException:
                    return 'There was an issue deleting the wrap-up of the mission'

                # Now we can delete the mission itself:
                from models.mission import Mission
                mission = Mission.query.filter_by(id=mission_id).first()

                try:
                    db.session.delete(mission)
                    db.session.commit()
                except BaseException:
                    return 'There was an issue deleting the mission'

                return redirect(request.url)

        else:
            from models.mission import Mission
            from routes.manage_missions import get_uploaded_missions, get_pam_missions_on_debriefing, get_il2stats_missions_not_linked

            return render_template(
                'missions.html',
                missions=Mission.query.all(),
                missionfiles=get_uploaded_missions(),
                pam_missions_on_debriefing=get_pam_missions_on_debriefing(),
                il2stats_missions_not_linked=get_il2stats_missions_not_linked())

    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
