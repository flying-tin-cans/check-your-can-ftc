from flask import Blueprint, render_template
from flask_login import login_required

index_blueprint = Blueprint(name='index', import_name=__name__)


@index_blueprint.route("/")
@index_blueprint.route("/index")
def index():
    return render_template(
        "index.html",
        processed_missions=get_processed_missions())


@index_blueprint.route("/get_processed_missions")
@login_required
def get_processed_missions_route():
    return render_template(
        "index.html",
        processed_missions=get_processed_missions())


def get_processed_missions():
    from models.mission import Mission

    processed_missions = Mission.query.filter_by(processed=True).all()

    return processed_missions
