from flask import Blueprint, render_template, redirect, url_for, flash
from flask_login import login_required, current_user

admin_blueprint = Blueprint(name='admin', import_name=__name__)


@admin_blueprint.route('/admin', methods=['GET'])
@login_required
def admin():
    if current_user.admin is True:
        from models.user import User
        return render_template('admin.html', users=User.query.all())

    else:
        flash('You do not have the permissions to access this page.')
        return redirect(url_for('index.index'))
