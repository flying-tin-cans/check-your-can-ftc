from enum import Enum


class Fate(Enum):
    NOT_CALCULATED = 0
    DISCONNECTED = 1
    LANDED = 2
    BAILED = 3
    CRASHED = 4
    DITCHED = 5
    POW = 6
    ESCAPED = 7
    DEAD_FRIENDLY_TERRITORY = 8
    DEAD_ENEMY_TERRITORY = 9
    RESCUED_FROM_FRIENDLY_FORCES = 10
    RESCUED_FROM_ENEMY_FORCES = 11
    DROWNED = 12
    DEAD_NEUTRAL_TERRITORY = 13

    def toString(self, fate):
        switcher = {
            Fate.NOT_CALCULATED: "not calculated",
            Fate.DISCONNECTED: "disconnected",
            Fate.LANDED: "landed",
            Fate.BAILED: "bailed",
            Fate.CRASHED: "crashed",
            Fate.DITCHED: "ditched",
            Fate.POW: "POW",
            Fate.ESCAPED: "escaped",
            Fate.DEAD_FRIENDLY_TERRITORY: "died within friendly territory",
            Fate.DEAD_ENEMY_TERRITORY: "died in enemy territory",
            Fate.RESCUED_FROM_FRIENDLY_FORCES: "rescued from friendly forces",
            Fate.RESCUED_FROM_ENEMY_FORCES: "rescued from enemy forces",
            Fate.DROWNED: "drowned",
            Fate.DEAD_NEUTRAL_TERRITORY: "died within neutral territory"
        }

        return switcher.get(fate, "Invalid input")
