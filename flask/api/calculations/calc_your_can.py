import json
from calculations.utils import Utils
from calculations.pilot import Pilot
from calculations.sortie import Sortie
from calculations.il2_stats import Il2_stats
from calculations.pam import Pam
from calculations.mission_frontline import Mission_frontline
from calculations.fate import Fate


DEBUG_PRINT_ON = False
WARNING_PRINT_ON = False
INFO_PRINT_ON = False

FACTOR_TO_KM = float(1 / 1000)  # Multiply to get km


class Calc_your_can:
    def __init__(self, db_config_file_path):
        self.db_config_file_path = db_config_file_path
        self.PAM_user = None
        self.PAM_password = None
        self.PAM_host = None
        self.PAM_database = None
        self.IL2stats_user = None
        self.IL2stats_password = None
        self.IL2stats_host = None
        self.IL2stats_database = None
        self.utils = Utils(DEBUG_PRINT_ON, WARNING_PRINT_ON, INFO_PRINT_ON)
        self.il2_stats = None
        self.pam = None
        self.frontline = Mission_frontline(self.utils)
        self.mission = None
        self.missionFile = None
        self.probabilities_filename = None
        self.path_to_maps = None
        self.sea_state = None
        self.dieppe_special = None

    def executeForMission(self, mission_il2stats, mission_pam, missionFile, probabilities_filename, path_to_maps):
        self.utils.infoPrint("Executing check-your-fate for mission:",
                             missionFile, "with il2_stats_db number:", mission_il2stats)
        self.mission = str(mission_il2stats)
        self.missionFile = missionFile
        self.probabilities_filename = probabilities_filename
        self.path_to_maps = path_to_maps
        if mission_pam == 39:
            self.dieppe_special = True
        else:
            self.dieppe_special = False

        self.sea_state = self.__getSeaStateFromFile()
        missionData = self.__getMissionData()
        missionFrontline = self.__getMissionFrontlineFromFile()
        missionFates = self.__calculateFate(mission_pam,
                                            missionData, missionFrontline["red"], missionFrontline["blue"])
        missionWrapUp = self.__calculateMissionWrapUp(missionFates)
        self.__disconnectFromDatabases()

        return missionFates, missionWrapUp, missionFrontline

    # TODO implement
    def recalc(self, fate_id):
        self.utils.infoPrint("Recalulcating fate: " + fate_id)

    def __calculateMissionWrapUp(self, missionFates):
        missionWrapUp = []

        # Get base units
        query = ("SELECT id AS pam_baseunitid, name AS pam_baseunit FROM base_unit;")
        query_return = self.pam.query(query)
        for (pam_baseunitid, pam_baseunit) in query_return:
            missionWrapUpForUnit = {
                "baseUnitName": pam_baseunit,
                "sorties": 0,
                "pilotHealthy": 0,
                "pilotWounded": 0,
                "pilotDead": 0,
                "aircraftUnharmed": 0,
                "aircraftDamaged": 0,
                "aircraftDestroyed": 0,
                "pilotLossPercentage": None,
                "aircraftLossPercentage": None
            }

            for fate in missionFates:
                if pam_baseunit == fate["pilot_unit"]:
                    missionWrapUpForUnit["sorties"] = missionWrapUpForUnit["sorties"] + 1

                    if "healthy" == fate["health"]:
                        missionWrapUpForUnit["pilotHealthy"] = missionWrapUpForUnit["pilotHealthy"] + 1
                    elif "wounded" == fate["health"]:
                        missionWrapUpForUnit["pilotWounded"] = missionWrapUpForUnit["pilotWounded"] + 1
                    elif "dead" == fate["health"]:
                        missionWrapUpForUnit["pilotDead"] = missionWrapUpForUnit["pilotDead"] + 1
                    else:
                        self.utils.errorPrint(
                            "Check this fate for the pilot health:")
                        self.utils.errorPrint(fate)

                    if "unharmed" == fate["aircraft_status"]:
                        missionWrapUpForUnit["aircraftUnharmed"] = missionWrapUpForUnit["aircraftUnharmed"] + 1
                    elif "damaged" == fate["aircraft_status"]:
                        missionWrapUpForUnit["aircraftDamaged"] = missionWrapUpForUnit["aircraftDamaged"] + 1
                    elif "destroyed" == fate["aircraft_status"]:
                        missionWrapUpForUnit["aircraftDestroyed"] = missionWrapUpForUnit["aircraftDestroyed"] + 1
                    else:
                        self.utils.errorPrint(
                            "Check this fate for the aircraft status:")
                        self.utils.errorPrint(fate)

            if 0 != missionWrapUpForUnit["sorties"]:
                missionWrapUpForUnit["pilotLossPercentage"] = "{:.1f}".format(
                    (missionWrapUpForUnit["pilotDead"] / missionWrapUpForUnit["sorties"]) * 100)
                missionWrapUpForUnit["aircraftLossPercentage"] = "{:.1f}".format(
                    (missionWrapUpForUnit["aircraftDestroyed"] / missionWrapUpForUnit["sorties"]) * 100)
                missionWrapUp.append(missionWrapUpForUnit)

        # TODO: faction calculation

        filenname = "outputs/mission_wrap_up_for_mission_" + self.mission + ".json"
        self.utils.saveDataToJson(filenname, missionWrapUp, False)
        return missionWrapUp

    def __getMissionData(self):
        self.__setDbAccessInfo()
        self.__connectToDatabases()
        pilot_list_pam = self.__getPamPilots()
        pilot_list_il2stats = self.__getIl2StatsPilots()
        pilot_list_merged = self.__mergePamAndIl2StatsPilots(
            pilot_list_pam, pilot_list_il2stats)
        pilot_list_mission = self.__throwOutNonParticipants(
            pilot_list_merged, self.mission)
        pilot_list_mission = self.__addSorties(
            pilot_list_mission, self.mission)
        filenname = "outputs/output_for_mission_" + self.mission + ".json"
        missionData = self.__saveDataToJson(
            filenname, pilot_list_mission, False)

        return missionData

    def __getMissionFrontlineFromFile(self):
        missionFrontline = self.frontline.executeForMissionFile(
            self.mission, self.missionFile, self.path_to_maps)
        return missionFrontline

    def __pilot_crashed(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly,
            enhanced_rescue_chance):
        # Check if il2 stats actually used the crashed position or the ditched
        # position
        if sortie["il2stats_position_crashed"] is not None:
            posX = float(sortie["il2stats_position_crashed"]["x"])
            posZ = float(sortie["il2stats_position_crashed"]["z"])
        elif sortie["il2stats_position_ditched"] is not None:
            posX = float(sortie["il2stats_position_ditched"]["x"])
            posZ = float(sortie["il2stats_position_ditched"]["z"])
        else:
            posX = 0
            posZ = 0
            self.utils.errorPrint("That should not happen")
            self.utils.errorPrint(sortie)
            self.utils.errorPrint(fate)

        fate["crashed_position"] = {"XPos": posX, "ZPos": posZ}

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if (("friendly" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["fate"] = Fate.CRASHED
        elif (("enemy" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["start_position"] = {"XPos": posX, "ZPos": posZ}
            self.__pilot_pow_or_escaped(
                fate, sortie, lines["shortesWaypointFriendly"], Fate.CRASHED)
        elif ("neutral" == lines["lines"]) and ((lines["over_sea"] is True)):
            # Dieppe special: if inside the Dieppe Zone, rescue is guaranteed
            if enhanced_rescue_chance:
                if self.utils.calculateEnhancedResuceChanceDieppe(posX, posZ):
                    fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES
                    fate["fate_detail"] = Fate.CRASHED
                    return None

            self.__pilot_drowned_pow_or_rescued(
                fate, sortie, lines["shortesWaypointFriendly"], lines["shortesWaypointNotFriendly"], Fate.CRASHED, self.sea_state)
        else:
            raise Exception("lines has invalid arguments")

    def __pilot_ditched(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly,
            enhanced_rescue_chance):
        posX = float(sortie["il2stats_position_ditched"]["x"])
        posZ = float(sortie["il2stats_position_ditched"]["z"])

        fate["ditched_position"] = {"XPos": posX, "ZPos": posZ}

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if (("friendly" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["fate"] = Fate.DITCHED
        elif (("enemy" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["start_position"] = {"XPos": posX, "ZPos": posZ}
            self.__pilot_pow_or_escaped(
                fate, sortie, lines["shortesWaypointFriendly"], Fate.DITCHED)
        elif ("neutral" == lines["lines"]) and ((lines["over_sea"] is True)):
            # Dieppe special: if inside the Dieppe Zone, rescue is guaranteed
            if enhanced_rescue_chance:
                if self.utils.calculateEnhancedResuceChanceDieppe(posX, posZ):
                    fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES
                    fate["fate_detail"] = Fate.DITCHED
                    return None

            self.__pilot_drowned_pow_or_rescued(
                fate, sortie, lines["shortesWaypointFriendly"], lines["shortesWaypointNotFriendly"], Fate.DITCHED, self.sea_state)
        else:
            raise Exception("lines has invalid arguments")

    def __pilot_died(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly):
        # Decide which position is the place the pilot died

        # Use the position end if it is not 0
        if ((sortie["il2stats_position_end"]["x"] != "0.0") and (
                sortie["il2stats_position_end"]["z"] != "0.0")):
            posX = float(sortie["il2stats_position_end"]["x"])
            posZ = float(sortie["il2stats_position_end"]["z"])
        # Next good option is landed
        elif (sortie["il2stats_position_landed"] is not None):
            posX = float(
                sortie["il2stats_position_landed"]["x"])
            posZ = float(
                sortie["il2stats_position_landed"]["z"])
        # Next good option is ditched
        elif (sortie["il2stats_position_ditched"] is not None):
            posX = float(
                sortie["il2stats_position_ditched"]["x"])
            posZ = float(
                sortie["il2stats_position_ditched"]["z"])
        # Next good option is bailout
        elif (sortie["il2stats_position_bailout"] is not None):
            posX = float(
                sortie["il2stats_position_bailout"]["x"])
            posZ = float(
                sortie["il2stats_position_bailout"]["z"])
        # Last option is crashed
        elif (sortie["il2stats_position_crashed"] is not None):
            posX = float(
                sortie["il2stats_position_crashed"]["x"])
            posZ = float(
                sortie["il2stats_position_crashed"]["z"])
        else:
            self.utils.errorPrint(
                "Sortie for",
                fate["pilot_name"],
                "which ended as dead has an error, please check")

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if "friendly" == lines["lines"]:
            fate["fate"] = Fate.DEAD_FRIENDLY_TERRITORY
        elif "neutral" == lines["lines"]:
            fate["fate"] = Fate.DEAD_NEUTRAL_TERRITORY
        elif "enemy" == lines["lines"]:
            fate["fate"] = Fate.DEAD_ENEMY_TERRITORY
        else:
            raise Exception("Lines not friendly, enemy or neutral")

        # Write position of death
        fate["dead_position"] = {
            "XPos": posX, "ZPos": posZ}

    def __pilot_pow_or_escaped(
            self,
            fate,
            sortie,
            shortesWaypointFriendly,
            fate_detail):
        # Landed behind enemy lines
        fate["target_position"] = shortesWaypointFriendly

        distanceToCoverInKm = shortesWaypointFriendly["Distance"] * FACTOR_TO_KM
        escape = self.utils.calculateEscape(
            distanceToCoverInKm, self.probabilities_filename, fate["wound"])

        # If captured en-route, calculate the point how far
        # the pilot came
        if escape["Captured-en-route"] is True:
            distanceCoveredInKm = escape["Distance-covered-if-captured"]
            capturedPoint = self.utils.caclulcatePointOfCapture(
                distanceCoveredInKm / FACTOR_TO_KM,
                fate["start_position"],
                fate["target_position"]
            )
            if escape["Captured-crossing-front"] is True:
                fate["captured_position"] = fate["target_position"]
            else:
                fate["captured_position"] = capturedPoint

            escape["Captured-en-route"]

            fate["fate"] = Fate.POW
        else:
            fate["fate"] = Fate.ESCAPED

        fate["fate_detail"] = fate_detail

    def __pilot_drowned_pow_or_rescued(self, fate, sortie, shortesWaypointFriendly, shortesWaypointNotFriendly, fate_detail, sea_state):
        # Swimming!
        fate["target_position"] = shortesWaypointFriendly
        distanceToCoverInKm = shortesWaypointFriendly["Distance"] * FACTOR_TO_KM
        asr = self.utils.calculateAsr(
            distanceToCoverInKm, self.probabilities_filename, fate["wound"], sea_state)

        # Second chance, maybe the enemy was friendly enough to rescue you?
        if asr["Drowned"]:
            fate["target_position"] = shortesWaypointNotFriendly
            distanceToCoverInKm = shortesWaypointNotFriendly["Distance"] * FACTOR_TO_KM

            asr = self.utils.calculateAsr(
                distanceToCoverInKm, self.probabilities_filename, fate["wound"], sea_state)

            # Not even the enemy saved you :(
            if asr["Drowned"]:
                fate["fate"] = Fate.DROWNED
            # You lucky bastard, enjoy your POW state
            else:
                fate["fate"] = Fate.RESCUED_FROM_ENEMY_FORCES

        # Hey, you are alive, nice one!
        else:
            fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES

        fate["fate_detail"] = fate_detail

    def __pilot_friendly_or_enemy_lines(
            self,
            fate,
            sortie,
            posX,
            posZ,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly):
        # Calculate point and distance for friendly lines
        shortesWaypointFriendly = self.utils.calculateDirectWayToLines(
            posX, posZ, missionFrontlineFriendly)

        # Calculate point and distance for not friendly lines
        shortesWaypointNotFriendly = self.utils.calculateDirectWayToLines(
            posX, posZ, missionFrontlineNotFriendly)

        # Check if we have to take into account the shorelines
        if (shorelineFriendly is not None) and (shorelineNotFriendly is not None):
            shortesShorelineFriendly = self.utils.calculateDirectWayToLines(
                posX, posZ, shorelineFriendly)

            shortesShorelineNotFriendly = self.utils.calculateDirectWayToLines(
                posX, posZ, shorelineNotFriendly)

            # Well, that will be a bit more complex now

            # Case 1: Over Friendly land: and
            # Case 2: On Friendly shore:
            if ((shortesShorelineFriendly["Distance"] < shortesShorelineNotFriendly["Distance"]) and
                (shortesWaypointFriendly["Distance"] < shortesWaypointNotFriendly["Distance"]) and
                    (shortesWaypointFriendly["Distance"] > shortesShorelineFriendly["Distance"])):
                return {
                    "lines": "friendly",
                    "over_sea": False,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": shortesShorelineFriendly,
                    "shortesShorelineNotFriendly": shortesShorelineNotFriendly}
            # Case 3: Water, friendly shore is nearer:
            elif ((shortesShorelineFriendly["Distance"] < shortesShorelineNotFriendly["Distance"]) and
                  (shortesWaypointFriendly["Distance"] < shortesWaypointNotFriendly["Distance"]) and
                  (shortesWaypointFriendly["Distance"] < shortesShorelineFriendly["Distance"])):
                return {
                    "lines": "neutral",
                    "over_sea": True,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": shortesShorelineFriendly,
                    "shortesShorelineNotFriendly": shortesShorelineNotFriendly}
            # Case 4: Water, enemy shore is nearer:
            elif ((shortesShorelineFriendly["Distance"] > shortesShorelineNotFriendly["Distance"]) and
                  (shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]) and
                  (shortesWaypointFriendly["Distance"] < shortesShorelineFriendly["Distance"])):
                return {
                    "lines": "neutral",
                    "over_sea": True,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": shortesShorelineFriendly,
                    "shortesShorelineNotFriendly": shortesShorelineNotFriendly}
            # Case 5: On Enemy shore:
            # Case 6: Over Enemy land:
            elif ((shortesShorelineFriendly["Distance"] > shortesShorelineNotFriendly["Distance"]) and
                  (shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]) and
                  (shortesWaypointNotFriendly["Distance"] < shortesShorelineNotFriendly["Distance"])):
                return {
                    "lines": "enemy",
                    "over_sea": False,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": shortesShorelineFriendly,
                    "shortesShorelineNotFriendly": shortesShorelineNotFriendly}
            else:
                raise Exception("Error in calculation")

        # No shorelines given, easy-peasy
        else:

            if shortesWaypointFriendly["Distance"] > shortesWaypointNotFriendly["Distance"]:
                return {
                    "lines": "enemy",
                    "over_sea": None,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": None,
                    "shortesShorelineNotFriendly": None}

            else:
                return {
                    "lines": "friendly",
                    "over_sea": None,
                    "shortesWaypointFriendly": shortesWaypointFriendly,
                    "shortesWaypointNotFriendly": shortesWaypointNotFriendly,
                    "shortesShorelineFriendly": None,
                    "shortesShorelineNotFriendly": None}

    def __pilot_bailed_out(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly,
            enhanced_rescue_chance):
        # Check if the landed position is available and use it:
        if None is not sortie["il2stats_position_landed"]:
            posX = float(
                sortie["il2stats_position_landed"]["x"])
            posZ = float(
                sortie["il2stats_position_landed"]["z"])
        # If not, use the bailout position
        else:
            posX = float(
                sortie["il2stats_position_bailout"]["x"])
            posZ = float(
                sortie["il2stats_position_bailout"]["z"])

        fate["bailout_position"] = {
            "XPos": posX, "ZPos": posZ}

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if (("friendly" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["fate"] = Fate.BAILED
        elif (("enemy" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["start_position"] = {"XPos": posX, "ZPos": posZ}
            self.__pilot_pow_or_escaped(
                fate, sortie, lines["shortesWaypointFriendly"], Fate.BAILED)
        elif ("neutral" == lines["lines"]) and ((lines["over_sea"] is True)):
            # Dieppe special: if inside the Dieppe Zone, rescue is guaranteed
            if enhanced_rescue_chance:
                if self.utils.calculateEnhancedResuceChanceDieppe(posX, posZ):
                    fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES
                    fate["fate_detail"] = Fate.DITCHED
                    return None

            self.__pilot_drowned_pow_or_rescued(
                fate, sortie, lines["shortesWaypointFriendly"], lines["shortesWaypointNotFriendly"], Fate.DITCHED, self.sea_state)
        else:
            raise Exception("lines has invalid arguments")

    def __pilot_landed(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly,
            enhanced_rescue_chance):
        posX = float(sortie["il2stats_position_landed"]["x"])
        posZ = float(sortie["il2stats_position_landed"]["z"])

        fate["landed_position"] = {
            "XPos": posX, "ZPos": posZ}

        # Dieppe special: if inside the Dieppe Zone, rescue is guaranteed
        if enhanced_rescue_chance:
            if self.utils.calculateEnhancedResuceChanceDieppe(posX, posZ):
                fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES
                return None

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if "friendly" == lines["lines"]:
            fate["fate"] = Fate.LANDED

        else:
            fate["start_position"] = {"XPos": posX, "ZPos": posZ}
            self.__pilot_pow_or_escaped(
                fate, sortie, lines["shortesWaypointFriendly"], Fate.LANDED)

    def __pilot_shotdown(
            self,
            fate,
            sortie,
            missionFrontlineFriendly,
            missionFrontlineNotFriendly,
            shorelineFriendly,
            shorelineNotFriendly,
            enhanced_rescue_chance):
        # Check if we want ditched or crashed as position
        fate_value = None

        if None is not sortie["il2stats_position_ditched"]:
            posX = float(
                sortie["il2stats_position_ditched"]["x"])
            posZ = float(
                sortie["il2stats_position_ditched"]["z"])
            fate["ditched_position"] = {"XPos": posX, "ZPos": posZ}
            fate_value = Fate.DITCHED

        elif None is not sortie["il2stats_position_crashed"]:
            posX = float(
                sortie["il2stats_position_crashed"]["x"])
            posZ = float(
                sortie["il2stats_position_crashed"]["z"])
            fate["crashed_position"] = {"XPos": posX, "ZPos": posZ}
            fate_value = Fate.CRASHED

        elif None is not sortie["il2stats_position_bailout"]:
            posX = float(
                sortie["il2stats_position_bailout"]["x"])
            posZ = float(
                sortie["il2stats_position_bailout"]["z"])
            fate["bailout_position"] = {"XPos": posX, "ZPos": posZ}
            fate_value = Fate.BAILED

        # Yes this can happen. Pilot landed with minor damaged succesfully and was apparently destroyed by an enemy afterwards. Bullshit.
        elif None is not sortie["il2stats_position_landed"]:
            posX = float(
                sortie["il2stats_position_landed"]["x"])
            posZ = float(
                sortie["il2stats_position_landed"]["z"])
            fate["landed_position"] = {"XPos": posX, "ZPos": posZ}
            fate_value = Fate.LANDED

        else:
            fate_value = Fate.NOT_CALCULATED

            self.utils.errorPrint(
                "Shotdown position error, check this sortie:")
            self.utils.errorPrint(sortie)
            raise Exception("Shotdown position error")

        # Dieppe special: if inside the Dieppe Zone, rescue is guaranteed
        if enhanced_rescue_chance:
            if self.utils.calculateEnhancedResuceChanceDieppe(posX, posZ):
                fate["fate"] = Fate.RESCUED_FROM_FRIENDLY_FORCES
                return None

        lines = self.__pilot_friendly_or_enemy_lines(
            fate, sortie, posX, posZ, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly)

        if (("friendly" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["fate"] = fate_value
        elif (("enemy" == lines["lines"]) and ((lines["over_sea"] is False) or (lines["over_sea"] is None))):
            fate["start_position"] = {"XPos": posX, "ZPos": posZ}
            self.__pilot_pow_or_escaped(
                fate, sortie, lines["shortesWaypointFriendly"], fate_value)
        elif ("neutral" == lines["lines"]) and ((lines["over_sea"] is True)):
            # TODO
            None
        else:
            raise Exception("lines has invalid arguments")

    def __calculateFate(self, mission_pam, missionData, missionFrontlineRed,
                        missionFrontlineBlue):
        # Check if we use the Normandy map (or in future any map with a shore-line)
        # if yes, we need to calculate the cold and wet death stuff

        # TODO: function def get_pam_map_id(pam_mission_id) already does this, but I'm too lazy right now
        # to do it proper

        # Get the map id which the campaign is using
        query = (
            "SELECT pam.campaign.platform AS map_id FROM pam.campaign RIGHT JOIN pam.mission ON pam.campaign.id = pam.mission.campaign_id WHERE pam.mission.id = " +
            str(mission_pam) +
            ";")
        query_return = self.pam.query(query)

        pam_map_id_list = []
        for (map_id) in query_return:
            pam_map_id_list.append(map_id)

        pam_map_id = pam_map_id_list[0]

        # Normandy is 7
        if 7 == pam_map_id[0]:
            # Time to get the shoreline
            shorelines = self.frontline.getShorelines()
            shoreline_england = shorelines["england"]
            shoreline_france = shorelines["france"]
        else:
            # shorelines are not used
            shoreline_england = None
            shoreline_france = None

        missionFates = []
        for pilot in missionData:
            for sortie in pilot["sorties"]:
                fate = {
                    "pam_id": pilot["pam_id"],
                    "pilot_name": pilot["pam_callsign"],
                    "pilot_unit": pilot["pam_baseunit"],
                    "fate": Fate.NOT_CALCULATED,
                    "fate_detail": None,
                    "health": sortie["il2stats_pilot_status"],
                    "wound": sortie["il2stats_wound"],
                    "aircraft_status": sortie["il2stats_aircraft_status"],
                    "damage": sortie["il2stats_damage"],
                    "takeoff_position": None,
                    "landed_position": None,
                    "ditched_position": None,
                    "bailout_position": None,
                    "target_position": None,
                    "captured_position": None,
                    "dead_position": None,
                    "start_position": None,
                    "crashed_position": None
                }

                if "not_takeoff" != sortie["il2stats_status"]:
                    if sortie["il2stats_position_takeoff"] is None:
                        # Corner-case: you can be smth else than "not_takeoff"
                        # but still can be "crashed"
                        pass
                    else:
                        # Write takeoff position
                        posX = float(sortie["il2stats_position_takeoff"]["x"])
                        posZ = float(sortie["il2stats_position_takeoff"]["z"])
                        fate["takeoff_position"] = {"XPos": posX, "ZPos": posZ}

                    # Check which frontline is friendly and which is not
                    if 1 == sortie["il2stats_coalition"]:
                        missionFrontlineFriendly = missionFrontlineRed
                        missionFrontlineNotFriendly = missionFrontlineBlue
                        shorelineFriendly = shoreline_england
                        shorelineNotFriendly = shoreline_france
                        if self.dieppe_special:
                            enhanced_rescue_chance = True
                        else:
                            enhanced_rescue_chance = False
                    elif 2 == sortie["il2stats_coalition"]:
                        missionFrontlineFriendly = missionFrontlineBlue
                        missionFrontlineNotFriendly = missionFrontlineRed
                        shorelineFriendly = shoreline_france
                        shorelineNotFriendly = shoreline_england
                        enhanced_rescue_chance = False
                    else:
                        self.utils.errorPrint(
                            "Sortie for",
                            pilot["pam_callsign"],
                            "has an error in the coalition, please check")

                    # Check pilot status
                    if (sortie["il2stats_pilot_status"] == "dead"):
                        self.__pilot_died(
                            fate,
                            sortie,
                            missionFrontlineFriendly,
                            missionFrontlineNotFriendly,
                            shorelineFriendly,
                            shorelineNotFriendly)

                    # Check if pilot bailed out (and did not die):
                    elif (sortie["ilstats_bailout"]) and (sortie["il2stats_pilot_status"] != "dead"):
                        self.__pilot_bailed_out(
                            fate, sortie, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly, enhanced_rescue_chance)

                    else:
                        # We check the il2stats_status:
                        if "crashed" == sortie["il2stats_status"]:
                            self.__pilot_crashed(
                                fate, sortie, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly, enhanced_rescue_chance)

                        elif "ditched" == sortie["il2stats_status"]:
                            self.__pilot_ditched(
                                fate, sortie, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly, enhanced_rescue_chance)

                        elif "in_flight" == sortie["il2stats_status"]:
                            fate["fate"] = Fate.DISCONNECTED

                        elif "landed" == sortie["il2stats_status"]:
                            self.__pilot_landed(
                                fate, sortie, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly, enhanced_rescue_chance)

                        elif "shotdown" == sortie["il2stats_status"]:
                            self.__pilot_shotdown(
                                fate, sortie, missionFrontlineFriendly, missionFrontlineNotFriendly, shorelineFriendly, shorelineNotFriendly, enhanced_rescue_chance)

                        else:
                            self.utils.errorPrint(
                                "Check sortie for", pilot["pam_callsign"])
                            self.utils.errorPrint(sortie)
                            raise Exception(
                                "Error, unexpected status while getting ilstats status")

                    missionFates.append(fate)

        filenname = "outputs/fates_for_mission_" + self.mission + ".json"
        self.utils.saveDataToJson(filenname, missionFates, False)

        return missionFates

    def __setDbAccessInfo(self):
        try:
            with open(self.db_config_file_path, "r") as json_file:
                secrets = json.load(json_file)

            self.PAM_user = secrets["PAM_user"]
            self.PAM_password = secrets["PAM_password"]
            self.PAM_host = secrets["PAM_host"]
            self.PAM_database = secrets["PAM_database"]
            self.IL2stats_user = secrets["IL2stats_user"]
            self.IL2stats_password = secrets["IL2stats_password"]
            self.IL2stats_host = secrets["IL2stats_host"]
            self.IL2stats_database = secrets["IL2stats_database"]

            self.utils.infoPrint("Got connection information from file")
        except FileNotFoundError as err:
            self.utils.errorPrint(err)
            self.utils.fatalError("Config read-out failed")

    def __connectToDatabases(self):
        self.il2_stats = Il2_stats(
            self.IL2stats_host,
            self.IL2stats_user,
            self.IL2stats_password,
            self.IL2stats_database,
            self.utils)
        self.il2_stats.createConnection()
        self.pam = Pam(self.PAM_host, self.PAM_user,
                       self.PAM_password, self.PAM_database, self.utils)
        self.pam.createConnection()

    def __disconnectFromDatabases(self):
        self.il2_stats.closeConnection()
        self.pam.closeConnection()

    def __saveDataToJson(self, filenname, pilot_list_mission, save_to_file):
        dictList = []

        for pilot in pilot_list_mission:
            dictList.append(pilot.toDictionary())

        if save_to_file is True:
            self.utils.saveDataToJson(filenname, dictList)
        else:
            pass

        return dictList

    def __getPamPilots(self):
        # Get pilot info
        query = ("SELECT member.id AS pam_id, member.callsign AS pam_callsign, member.boxid AS pam_boxid, current_unit_members.base_unit_id AS pam_baseunitid FROM member INNER JOIN current_unit_members ON member.id = current_unit_members.id;")
        query_return = self.pam.query(query)
        pilot_list = []
        for (pam_id, pam_callsign, pam_boxid, pam_baseunitid) in query_return:
            pilot_list.append(Pilot(pam_id, pam_callsign,
                              pam_boxid, pam_baseunitid))

        # Get base unit name
        query = ("SELECT id AS pam_baseunitid, name AS pam_baseunit FROM base_unit;")
        query_return = self.pam.query(query)
        for (pam_baseunitid, pam_baseunit) in query_return:
            for pilot in pilot_list:
                if pam_baseunitid == pilot.pam_baseunitid:
                    pilot.pam_baseunit = pam_baseunit

        return pilot_list

    def __getIl2StatsPilots(self):
        query_results = []
        query = (
            "SELECT id AS il2stats_id, uuid AS il2stats_boxid, nickname AS il2stats_nickname FROM profiles;")
        query_return = self.il2_stats.query(query)
        for (il2stats_id, il2stats_boxid, il2stats_nickname) in query_return:
            query_results.append({"il2stats_id": il2stats_id,
                                  "il2stats_boxid": il2stats_boxid,
                                  "il2stats_nickname": il2stats_nickname})
        return query_results

    def __mergePamAndIl2StatsPilots(self, pilot_list_pam, pilot_list_il2stats):
        for pilot in pilot_list_pam:
            for x in pilot_list_il2stats:
                if pilot.pam_boxid == x["il2stats_boxid"]:
                    pilot.il2stats_id = x["il2stats_id"]
                    pilot.il2stats_boxid = x["il2stats_boxid"]
                    pilot.il2stats_nickname = x["il2stats_nickname"]
                    self.utils.debugPrint(
                        "Found pilot:",
                        pilot.pam_callsign,
                        "with il2_stats_db ID:",
                        pilot.il2stats_id)
                    break
        return pilot_list_pam

    def __throwOutNonParticipants(self, pilot_list_merged, mission):
        query_results = []

        # Sort out the pilots who did not flew this mission:
        query = ("SELECT id AS il2stats_sortie_id, profile_id AS il2stats_id, status AS il2stats_status, aircraft_status AS il2stats_aircraft_status, bot_status AS il2stats_pilot_status, is_bailout AS ilstats_bailout, coalition AS il2stats_coalition, wound AS il2stats_wound, damage AS il2stats_damage FROM sorties WHERE mission_id = " + mission + ";")

        query_return = self.il2_stats.query(query)

        pilot_list_mission = []

        # Search for the pilots in the sorties
        for (
            il2stats_sortie_id,
            il2stats_id,
            il2stats_status,
            il2stats_aircraft_status,
            il2stats_pilot_status,
            ilstats_bailout,
            il2stats_coalition,
            il2stats_wound,
                il2stats_damage) in query_return:
            query_results.append({
                "il2stats_sortie_id": il2stats_sortie_id,
                "il2stats_id": il2stats_id,
                "il2stats_status": il2stats_status,
                "il2stats_aircraft_status": il2stats_aircraft_status,
                "il2stats_pilot_status": il2stats_pilot_status,
                "ilstats_bailout": ilstats_bailout,
                "il2stats_coalition": il2stats_coalition,
                "il2stats_wound": il2stats_wound,
                "il2stats_damage": il2stats_damage})

        for pilot in pilot_list_merged:
            for x in query_results:
                if pilot.il2stats_id == x["il2stats_id"]:
                    pilot.sorties.append(
                        Sortie(
                            x["il2stats_sortie_id"],
                            x["il2stats_coalition"],
                            x["il2stats_status"],
                            x["il2stats_aircraft_status"],
                            x["il2stats_pilot_status"],
                            x["ilstats_bailout"],
                            x["il2stats_wound"],
                            x["il2stats_damage"]))
                    if pilot not in pilot_list_mission:
                        self.utils.debugPrint(
                            "First sortie found for", pilot.pam_callsign)
                        pilot_list_mission.append(pilot)
                    self.utils.debugPrint(
                        "Found sortie for:",
                        pilot.pam_callsign,
                        "with il2stats_id",
                        pilot.il2stats_id,
                        "with il2_stats_db sortie ID",
                        x["il2stats_sortie_id"])

        return pilot_list_mission

    def __addSorties(self, pilot_list_mission, mission):
        query_results = []

        # Add information to the sorties
        query_results.clear()
        query = (
            "SELECT act_sortie_id AS il2stats_sortie_id, type AS il2stats_type, extra_data AS il2stats_position, id AS il2stats_log_entrie_id FROM log_entries WHERE mission_id = " +
            mission +
            " AND cact_object_id IS NULL;")

        query_return = self.il2_stats.query(query)

        for (il2stats_sortie_id, il2stats_type, il2stats_position,
             il2stats_log_entrie_id) in query_return:
            query_results.append(
                {
                    "il2stats_sortie_id": il2stats_sortie_id,
                    "il2stats_type": il2stats_type,
                    "il2stats_position": il2stats_position,
                    "il2stats_log_entrie_id": il2stats_log_entrie_id})

        for pilot in pilot_list_mission:
            self.utils.debugPrint("Checking for", pilot.pam_callsign)
            for sortie in pilot.sorties:
                self.utils.debugPrint(
                    "Checking sortie",
                    sortie.il2stats_sortie_id,
                    "for",
                    pilot.pam_callsign)
                for x in query_results:
                    if sortie.il2stats_sortie_id == x["il2stats_sortie_id"]:
                        self.utils.debugPrint(
                            "Found sortie match", sortie.il2stats_sortie_id)
                        if "bailout" == x["il2stats_type"]:
                            sortie.il2stats_position_bailout = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "bailed out at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "crashed" == x["il2stats_type"]:
                            sortie.il2stats_position_crashed = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Aircraft of Pilot",
                                pilot.pam_callsign,
                                "crashed at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "ditched" == x["il2stats_type"]:
                            sortie.il2stats_position_ditched = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "ditched at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "end" == x["il2stats_type"]:
                            sortie.il2stats_position_end = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "ended sortie at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "landed" == x["il2stats_type"]:
                            sortie.il2stats_position_landed = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "landed at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "respawn" == x["il2stats_type"]:
                            sortie.il2stats_position_respawn = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "(re)spawned at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        elif "takeoff" == x["il2stats_type"]:
                            sortie.il2stats_position_takeoff = self.utils.translatePosData(
                                x["il2stats_position"])
                            self.utils.debugPrint(
                                "Pilot",
                                pilot.pam_callsign,
                                "took off at",
                                x["il2stats_position"],
                                "in il2_stats_db sortie ID",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])
                        else:
                            self.utils.warningPrint(
                                "Something went wrong for",
                                pilot.pam_callsign,
                                "in sortie",
                                sortie.il2stats_sortie_id,
                                "with il2stats_log_entrie_id",
                                x["il2stats_log_entrie_id"])

        return pilot_list_mission

    def __getSeaStateFromFile(self):

        with open(self.missionFile, 'r', encoding='utf-8') as missionfile:
            data = missionfile.read()

        seaStateLine = data.find('SeaState')
        # Delete the string stuff before it
        data = data[seaStateLine:]
        # Find the end of Seastate
        seaStateLineEnd = data.find('Turbulence')
        # Delete the rest of the string
        data = data[:seaStateLineEnd]
        # Get rid of the spaces
        data = data.replace(' ', '')
        # Get rid of the semicolons
        data = data.replace(';', '')
        # Get rid of the brackets
        data = data.replace('[', '')
        data = data.replace(']', '')
        # Convert every line into a list entry
        data = list(data.split('\n'))

        seaState = int(data[0].replace('SeaState=', ''))

        return seaState
