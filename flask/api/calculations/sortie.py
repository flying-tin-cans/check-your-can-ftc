class Sortie:
    def __init__(
            self,
            il2stats_sortie_id,
            il2stats_coalition,
            il2stats_status,
            il2stats_aircraft_status,
            il2stats_pilot_status,
            ilstats_bailout,
            il2stats_wound,
            il2stats_damage):
        self.il2stats_sortie_id = il2stats_sortie_id
        self.il2stats_coalition = il2stats_coalition
        self.il2stats_status = il2stats_status
        self.il2stats_aircraft_status = il2stats_aircraft_status
        self.il2stats_pilot_status = il2stats_pilot_status
        self.ilstats_bailout = ilstats_bailout
        self.il2stats_wound = il2stats_wound
        self.il2stats_damage = il2stats_damage
        self.il2stats_position_bailout = None
        self.il2stats_position_crashed = None
        self.il2stats_position_ditched = None
        self.il2stats_position_end = None
        self.il2stats_position_landed = None
        self.il2stats_position_respawn = None
        self.il2stats_position_takeoff = None

    def toDictionary(self):
        sortie_dict = {
            "il2stats_sortie_id": self.il2stats_sortie_id,
            "il2stats_coalition": self.il2stats_coalition,
            "il2stats_status": self.il2stats_status,
            "il2stats_aircraft_status": self.il2stats_aircraft_status,
            "il2stats_pilot_status": self.il2stats_pilot_status,
            "ilstats_bailout": self.ilstats_bailout,
            "il2stats_wound": self.il2stats_wound,
            "il2stats_damage": self.il2stats_damage,
            "il2stats_position_respawn": None,
            "il2stats_position_takeoff": None,
            "il2stats_position_bailout": None,
            "il2stats_position_crashed": None,
            "il2stats_position_ditched": None,
            "il2stats_position_landed": None,
            "il2stats_position_end": None
        }

        if None is not self.il2stats_position_bailout:
            sortie_dict["il2stats_position_bailout"] = self.il2stats_position_bailout.toDictionary()
        if None is not self.il2stats_position_crashed:
            sortie_dict["il2stats_position_crashed"] = self.il2stats_position_crashed.toDictionary()
        if None is not self.il2stats_position_ditched:
            sortie_dict["il2stats_position_ditched"] = self.il2stats_position_ditched.toDictionary()
        if None is not self.il2stats_position_end:
            sortie_dict["il2stats_position_end"] = self.il2stats_position_end.toDictionary(
            )
        if None is not self.il2stats_position_landed:
            sortie_dict["il2stats_position_landed"] = self.il2stats_position_landed.toDictionary()
        if None is not self.il2stats_position_respawn:
            sortie_dict["il2stats_position_respawn"] = self.il2stats_position_respawn.toDictionary()
        if None is not self.il2stats_position_takeoff:
            sortie_dict["il2stats_position_takeoff"] = self.il2stats_position_takeoff.toDictionary()

        return sortie_dict
