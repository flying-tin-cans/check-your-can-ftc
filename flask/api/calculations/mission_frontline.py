class Frontline_points:
    def __init__(self, XPos, YPos, ZPos, Index, Target):
        self.XPos = XPos
        self.YPos = YPos
        self.ZPos = ZPos
        self.Index = Index
        self.Target = Target
        self.pointerNext = None

    def __getitem__(self, key):
        return self

    def toString(self):
        return (
            "Index:", self.Index,
            "Target:", self.Target,
            "XPos:", self.XPos,
            "YPos:", self.YPos,
            "ZPos:", self.ZPos,
            "pointerNext:", self.pointerNext
        )

    def toDict(self):
        return (
            {
                "XPos": self.XPos,
                "YPos": self.YPos,
                "ZPos": self.ZPos
            }
        )


class Mission_frontline:
    def __init__(self, cUtils):
        self.frontlinePointsRed = []
        self.frontlinePointsBlue = []
        self.shorelinePointsFrance = []
        self.shorelinePointsEngland = []
        self.utils = cUtils
        self.mission = None
        self.filename = None
        self.path_to_maps = None

    def executeForMissionFile(self, mission, filenname, path_to_maps):
        # Clear list
        self.frontlinePointsRed = []
        self.frontlinePointsBlue = []

        self.mission = mission
        self.filename = filenname
        self.path_to_maps = path_to_maps

        frontline = {
            "red": self.__getFrontline("red"),
            "blue": self.__getFrontline("blue")
        }

        return frontline

    def getShorelines(self):
        # Clear list
        self.shorelinePointsFrance = []
        self.shorelinePointsEngland = []

        shorelines = {
            "france": self.__getShoreline("france"),
            "england": self.__getShoreline("england")
        }

        return shorelines

    def __getShoreline(self, land):
        # TODO: add more shorelines
        path_to_map = self.path_to_maps + "/normandy-early/Shore Lines - Normandy.Mission"
        with open(path_to_map, 'r', encoding='utf-8') as missionfile:
            data = missionfile.read()

        # Get start of the shoreline data
        if "france" == land:
            startShoreLines = data.find(
                'Group\n{\n  Name = \"Shore Line - France\";')
            shorelinePoints = self.shorelinePointsFrance
            filennameJson = "outputs/shoreline_france_for_mission_" + self.mission + ".json"
        if "england" == land:
            startShoreLines = data.find(
                'Group\n{\n  Name = \"Shore Line - England\";')
            shorelinePoints = self.shorelinePointsEngland
            filennameJson = "outputs/shoreline_england_for_mission_" + self.mission + ".json"

        # Delete the string stuff before it
        data = data[startShoreLines:]
        # Find the start of the next Group
        endShoreLines = data.find('\nGroup')
        # Delete the rest of the string
        data = data[:endShoreLines]
        # Get rid of the spaces
        data = data.replace(' ', '')
        # Get rid of the semicolons
        data = data.replace(';', '')
        # Get rid of the brackets
        data = data.replace('[', '')
        data = data.replace(']', '')
        # Convert every line into a list entry
        data = list(data.split('\n'))

        shorelinePoints = []

        for index, content in enumerate(data, start=0):
            if content.startswith('MCU_Icon'):
                XPos = data[index + 5].replace('XPos=', '')
                YPos = data[index + 6].replace('YPos=', '')
                ZPos = data[index + 7].replace('ZPos=', '')
                Index = data[index + 2].replace('Index=', '')
                Target = data[index + 3].replace('Targets=', '')

                Index = int(Index)
                # Target can be empty (this means this is the last point of the
                # frontline)
                if Target != '':
                    Target = int(Target)
                else:
                    # Empty statement for debugging
                    None

                shorelinePoints.append(Frontline_points(
                    XPos, YPos, ZPos, Index, Target))

        # Fill in the targets
        for shorelinePoint in shorelinePoints:
            if (shorelinePoint.Target != '') and (
                    shorelinePoint.pointerNext is None):
                shorelinePoint.pointerNext = self.__findTarget(
                    shorelinePoints, shorelinePoint.Target)

        # Sort the frontline
        shorelinePoints = self.__createSortedList(shorelinePoints)

        # Make data JSON serializable
        shorelinePointsForJson = []
        for shorelinePoint in shorelinePoints:
            shorelinePointsForJson.append(shorelinePoint.toDict())

        self.utils.saveDataToJson(filennameJson, shorelinePointsForJson, False)

        return shorelinePointsForJson

    def __getFrontline(self, coalition):
        with open(self.filename, 'r', encoding='utf-8') as missionfile:
            data = missionfile.read()

        # Get start of the frontline data
        if "red" == coalition:
            startFrontLines = data.find(
                'Group\n{\n  Name = \"Front Lines - Allies\";')
            frontlinePoints = self.frontlinePointsRed
            filennameJson = "outputs/frontline_red_for_mission_" + self.mission + ".json"
        if "blue" == coalition:
            startFrontLines = data.find(
                'Group\n{\n  Name = \"Front Lines - Axis\";')
            frontlinePoints = self.frontlinePointsBlue
            filennameJson = "outputs/frontline_blue_for_mission_" + self.mission + ".json"

        # Delete the string stuff before it
        data = data[startFrontLines:]
        # Find the start of the next Group
        endFrontLines = data.find('\nGroup')
        # Delete the rest of the string
        data = data[:endFrontLines]
        # Get rid of the spaces
        data = data.replace(' ', '')
        # Get rid of the semicolons
        data = data.replace(';', '')
        # Get rid of the brackets
        data = data.replace('[', '')
        data = data.replace(']', '')
        # Convert every line into a list entry
        data = list(data.split('\n'))

        frontlinePoints = []

        for index, content in enumerate(data, start=0):
            if content.startswith('MCU_Icon'):
                XPos = data[index + 5].replace('XPos=', '')
                YPos = data[index + 6].replace('YPos=', '')
                ZPos = data[index + 7].replace('ZPos=', '')
                Index = data[index + 2].replace('Index=', '')
                Target = data[index + 3].replace('Targets=', '')

                Index = int(Index)
                # Target can be empty (this means this is the last point of the
                # frontline)
                if Target != '':
                    Target = int(Target)

                frontlinePoints.append(Frontline_points(
                    XPos, YPos, ZPos, Index, Target))

        # Fill in the targets
        for frontlinePoint in frontlinePoints:
            if (frontlinePoint.Target != '') and (
                    frontlinePoint.pointerNext is None):
                frontlinePoint.pointerNext = self.__findTarget(
                    frontlinePoints, frontlinePoint.Target)

        # Sort the frontline
        frontlinePoints = self.__createSortedList(frontlinePoints)

        # Make data JSON serializable
        frontlinePointsForJson = []
        for frontlinePoint in frontlinePoints:
            frontlinePointsForJson.append(frontlinePoint.toDict())

        self.utils.saveDataToJson(filennameJson, frontlinePointsForJson, False)

        return frontlinePointsForJson

    def __findTarget(self, givenList, target):
        for listItem in givenList:
            if listItem.Index == target:
                return listItem

    def __createSortedList(self, unsortedList):
        sortedList = []
        item = unsortedList[0]
        while item.pointerNext is not None:
            sortedList.append(item)
            item = item.pointerNext
        return sortedList
