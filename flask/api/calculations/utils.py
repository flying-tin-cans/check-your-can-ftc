import sys
import os
import json
from shapely.geometry import LineString as shLs
from shapely.geometry import Point as shPt
from shapely.geometry import Polygon as shPoly
import random
import numpy as np


class Position:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def toDictionary(self):
        position_dict = {
            "x": self.x,
            "y": self.y,
            "z": self.z
        }

        return position_dict


class Utils:
    def __init__(self, debug_print, warning_print, info_print):
        self.debug_print = debug_print
        self.warning_print = warning_print
        self.info_print = info_print

    def debugPrint(self, *messages):
        if self.debug_print is True:
            print("DEBUG:", *messages)

    def warningPrint(self, *messages):
        if self.warning_print is True:
            print("WARNING:", *messages)

    def errorPrint(self, *messages):
        print("ERROR:", *messages)

    def infoPrint(self, *messages):
        if self.info_print is True:
            print("INFO:", *messages)

    def translatePosData(self, il2stats_position):
        x = il2stats_position["pos"]["x"]
        y = il2stats_position["pos"]["y"]
        z = il2stats_position["pos"]["z"]
        pos = Position(x, y, z)
        return pos

    def fatalError(self, errormessages):
        sys.exit(errormessages)

    def saveDataToJson(self, filenname, data, save_to_file):
        if save_to_file is True:
            if os.path.exists(filenname):
                os.remove(filenname)

            with open(filenname, 'w') as outfile:
                json.dump(data, outfile, indent=4)
        else:
            pass

    def calculateDirectWayToLines(self, pointX, pointZ, line):
        pointX = float(pointX)
        pointZ = float(pointZ)

        p = shPt(pointX, pointZ)
        distances = []
        for index, content in enumerate(line, start=0):
            XPos = float(line[index]["XPos"])
            ZPos = float(line[index]["ZPos"])
            XPosNext = float(line[index + 1]["XPos"])
            ZPosNext = float(line[index + 1]["ZPos"])
            fabricated_line = shLs([(XPos, ZPos), (XPosNext, ZPosNext)])
            dist = p.distance(fabricated_line)
            point = fabricated_line.interpolate(dist)

            distances.append({'distance': dist, 'point': point})

            if index + 2 == len(line):
                break

        shortestDistance = distances[0]['distance']
        pointForCrossing = distances[0]['point']

        for distance in distances:
            if distance['distance'] < shortestDistance:
                shortestDistance = distance['distance']
                pointForCrossing = distance['point']

        pointForCrossing = {"XPos": float(pointForCrossing.x), "ZPos": float(
            pointForCrossing.y), "Distance": float(shortestDistance)}

        return pointForCrossing

    def caclulcatePointOfCapture(
            self, distanceCovered, startPoint, targetPoint):
        vectorStart = np.array([startPoint["XPos"], 0, startPoint["ZPos"]])
        self.infoPrint("vectorStart:", vectorStart)

        vector = np.array([targetPoint["XPos"] - startPoint["XPos"],
                          0, targetPoint["ZPos"] - startPoint["ZPos"]])
        self.infoPrint("vector:", vector)

        unitVector = vector / np.linalg.norm(vector)
        self.infoPrint("unitVector:", unitVector)

        PointOfCapture = vectorStart + (distanceCovered * unitVector)
        self.infoPrint("PointOfCapture:", PointOfCapture)

        return {"XPos": float(PointOfCapture[0]), "ZPos": float(
            PointOfCapture[2])}

    def calculateEscape(
            self,
            distanceToCoverInKm,
            probabilities_filename,
            pilot_wound):
        self.infoPrint("******* Calculate Escape *******")
        escape = {"Captured-en-route": False, "Captured-crossing-front": False,
                  "Distance-covered-if-captured": None}

        with open(probabilities_filename) as json_file:
            probabilities = json.load(json_file)

        for probability in probabilities:
            if "probabilityToCover100m" == probability["name"]:
                Xkm = probability["values"]["Xkm"]
                probabilityToCoverXkm = probability["values"]["probabilityToCoverXkm"]
            if "probabilityToCrossFrontline" == probability["name"]:
                probabilityToCrossFrontline = probability["values"]["probabilityToCrossFrontline"]

        self.infoPrint("distanceToCoverInKm", distanceToCoverInKm)

        # Calculate probabilityToCover100m
        probabilityToCover100m = probabilityToCoverXkm**(1 / (Xkm * 10))
        self.infoPrint("probabilityToCover100m", probabilityToCover100m)

        self.infoPrint("probabilityToCrossFrontline",
                       probabilityToCrossFrontline)

        # Probability to make it home is probabilityToCover100m ^
        # distanceIn100m
        probabilityToMakeItHome = probabilityToCover100m ** (
            distanceToCoverInKm * 10)
        self.infoPrint("probabilityToMakeItHome", probabilityToMakeItHome)

        # Lower his chances depending on the pilot wound (0-100).
        # 0 means no wounds and the chance does not get lowered,
        # 99 means pilot is heavily wounded and his chances are lowered by another 99%
        # 100 means the pilot should be dead, this can happen in the logs
        # (and I assunme this is a bug Date: 20220204) so we just set it to 99.9

        if float(pilot_wound) >= 100:
            pilot_wound = 99.9
        else:
            pass

        probabilityToMakeItHome = probabilityToMakeItHome * \
            ((100 - float(pilot_wound)) / 100)

        # Did he made it?
        diceRollToFrontline = random.uniform(0, 1)
        self.infoPrint("dice_roll for getting to frontline",
                       diceRollToFrontline)

        if diceRollToFrontline < probabilityToMakeItHome:
            self.infoPrint("Pilot made it to frontline")

            escape["Captured-en-route"] = False

            # Check if he crossed the frontline
            diceRollOverFrontline = random.uniform(0, 1)
            self.infoPrint("dice_roll for getting over frontline",
                           diceRollOverFrontline)

            if diceRollOverFrontline > probabilityToCrossFrontline:
                self.infoPrint("Pilot made it to friendly territory")
                escape["Captured-crossing-front"] = False
            else:
                self.infoPrint("Pilot got captured crossing the frontline")
                escape["Captured-crossing-front"] = True
        else:
            self.infoPrint("Pilot got captured on the way to the frontline")
            distanceCoveredInKm = 10 * \
                (np.log(diceRollToFrontline) / np.log(probabilityToMakeItHome))
            self.infoPrint("distanceCoveredInKm", distanceCoveredInKm)
            escape["Distance-covered-if-captured"] = distanceCoveredInKm
            escape["Captured-en-route"] = True

        return escape

    def calculateAsr(
            self,
            distanceToCoverInKm,
            probabilities_filename,
            pilot_wound,
            sea_state):
        self.infoPrint("******* Calculate ASR *******")
        asr = {"Drowned": False}

        with open(probabilities_filename) as json_file:
            probabilities = json.load(json_file)

        for probability in probabilities:
            if "probabilityToGetAsr" == probability["name"]:
                Xkm = probability["values"]["Xkm"]
                probabilityToCoverXkm = probability["values"]["probabilityToCoverXkm"]

        self.infoPrint("distanceToCoverInKm", distanceToCoverInKm)

        # Calculate probabilityToCover100m
        probabilityToCover100m = probabilityToCoverXkm**(1 / (Xkm * 10))
        self.infoPrint("probabilityToCover100m", probabilityToCover100m)

        # Probability to make it home is probabilityToCover100m ^
        # distanceIn100m
        probabilityToGetAsr = probabilityToCover100m ** (
            distanceToCoverInKm * 10)
        self.infoPrint("probabilityToGetAsr", probabilityToGetAsr)

        # Lower his chances depending on the pilot wound (0-100).
        # 0 means no wounds and the chance does not get lowered,
        # 99 means pilot is heavily wounded and his chances are lowered by another 99%
        # 100 means the pilot should be dead, this can happen in the logs
        # (and I assunme this is a bug Date: 20220204) so we just set it to 99.9

        if float(pilot_wound) >= 100:
            pilot_wound = 99.9
        else:
            pass

        probabilityPilotWound = ((100 - float(pilot_wound)) / 100)

        # Sea state, 0 is light, 9 is max.
        probabilitySeaState = ((100 - ((sea_state) * 10)) / 100)

        probabilityToGetAsr = probabilityToGetAsr * \
            probabilityPilotWound * probabilitySeaState

        # Did he made it?
        diceRollToFrontline = random.uniform(0, 1)
        self.infoPrint("dice_roll for getting to frontline",
                       diceRollToFrontline)

        if diceRollToFrontline < probabilityToGetAsr:
            self.infoPrint("Pilot got rescued")
            asr["Drowned"] = False
        else:
            self.infoPrint("Pilot drowned")
            asr["Drowned"] = True

        return asr

    def calculateEnhancedResuceChanceDieppe(
            self,
            posX,
            posZ):
        DieppeZone = shPoly(
            [
                (221932, 229770),
                (211967, 229770),
                (201104, 239305),
                (200986, 249817),
                (206262, 253569),
                (215328, 254898),
                (221971, 259822)
            ]
        )

        Point = shPt(posX, posZ)

        return Point.within(DieppeZone)
