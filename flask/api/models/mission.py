from setup import db


class Mission(db.Model):
    __tablename__ = 'missions'
    id = db.Column(db.Integer, primary_key=True)
    pam_campaign_id = db.Column(db.Integer, nullable=True)
    pam_id = db.Column(db.Integer, nullable=False)
    pam_name = db.Column(db.String(50), nullable=False)
    il2stats_id = db.Column(db.Integer, unique=True, nullable=False)
    filename = db.Column(db.String(50), nullable=False)
    frontline_red = db.Column(db.Text)
    frontline_blue = db.Column(db.Text)
    processed = db.Column(db.Boolean, nullable=False)

    def __repr__(self):
        return '<Mission %r>' % self.id
