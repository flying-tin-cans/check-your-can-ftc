from setup import db
from calculations.fate import Fate


class Fate(db.Model):
    __tablename__ = 'fates'
    id = db.Column(db.Integer, primary_key=True)
    mission_id = db.Column(db.Integer, db.ForeignKey('missions.id'))
    name = db.Column(db.String(50), nullable=False)
    pam_id = db.Column(db.Integer, nullable=False)
    unit = db.Column(db.String(50), nullable=False)
    fate = db.Column(db.Enum(Fate), nullable=False)
    fate_detail = db.Column(db.Enum(Fate), nullable=True)
    health = db.Column(db.String(50), nullable=False)
    wound = db.Column(db.Float, nullable=True)
    aircraft_status = db.Column(db.String(50), nullable=False)
    damage = db.Column(db.Float, nullable=True)
    takeoff_position_x = db.Column(db.Float, nullable=True)
    takeoff_position_z = db.Column(db.Float, nullable=True)
    landed_position_x = db.Column(db.Float, nullable=True)
    landed_position_z = db.Column(db.Float, nullable=True)
    ditched_position_x = db.Column(db.Float, nullable=True)
    ditched_position_z = db.Column(db.Float, nullable=True)
    crashed_position_x = db.Column(db.Float, nullable=True)
    crashed_position_z = db.Column(db.Float, nullable=True)
    bailout_position_x = db.Column(db.Float, nullable=True)
    bailout_position_z = db.Column(db.Float, nullable=True)
    start_position_x = db.Column(db.Float, nullable=True)
    start_position_z = db.Column(db.Float, nullable=True)
    target_position_x = db.Column(db.Float, nullable=True)
    target_position_z = db.Column(db.Float, nullable=True)
    captured_position_x = db.Column(db.Float, nullable=True)
    captured_position_z = db.Column(db.Float, nullable=True)
    dead_position_x = db.Column(db.Float, nullable=True)
    dead_position_z = db.Column(db.Float, nullable=True)

    def __repr__(self):
        return '<Fate %r>' % self.id
