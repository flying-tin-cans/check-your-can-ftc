from setup import db
from flask_login import UserMixin


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    admin = db.Column(db.Boolean, nullable=False)
    campaign_team = db.Column(db.Boolean, nullable=False)
    staff = db.Column(db.Boolean, nullable=False)
