from setup import db


class Wrap_Up(db.Model):
    __tablename__ = 'wrap_ups'
    id = db.Column(db.Integer, primary_key=True)
    mission_id = db.Column(
        db.Integer,
        db.ForeignKey('missions.id'),
        nullable=False)
    base_unit_name = db.Column(db.Text, nullable=False)
    sorties = db.Column(db.Integer)
    pilotHealthy = db.Column(db.Integer)
    pilotWounded = db.Column(db.Integer)
    pilotDead = db.Column(db.Integer)
    aircraftUnharmed = db.Column(db.Integer)
    aircraftDamaged = db.Column(db.Integer)
    aircraftDestroyed = db.Column(db.Integer)
    pilotLossPercentage = db.Column(db.Integer)
    aircraftLossPercentage = db.Column(db.Integer)

    def __repr__(self):
        return '<Wrap_Up %r>' % self.id
