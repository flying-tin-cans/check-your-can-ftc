from setup import db, app
from werkzeug.security import generate_password_hash


@app.before_first_request
def create_database():
    from models.fate import Fate  # noqa: F401
    from models.mission import Mission  # noqa: F401
    from models.wrap_up import Wrap_Up  # noqa: F401
    from models.user import User  # noqa: F401
    db.create_all()

    # create the default users. Hash the password so the plaintext
    # version isn't saved.
    # TODO: change the passwords!
    user_admin = User(
        email="admin@ftc-cyc.com",
        name="admin",
        password=generate_password_hash(
            "password",
            method='sha256'),
        admin=True,
        campaign_team=True,
        staff=True)

    user_campaign_team = User(
        email="campaign_team@ftc-cyc.com",
        name="campaign_team",
        password=generate_password_hash(
            "password",
            method='sha256'),
        admin=False,
        campaign_team=True,
        staff=False)

    user_staff = User(
        email="staff@ftc-cyc.com",
        name="staff",
        password=generate_password_hash(
            "password",
            method='sha256'),
        admin=False,
        campaign_team=False,
        staff=True)

    user = User.query.filter_by(email="admin@ftc-cyc.com").first()
    if user:
        pass
    else:
        db.session.add(user_admin)

    user = User.query.filter_by(email="campaign_team@ftc-cyc.com").first()
    if user:
        pass
    else:
        db.session.add(user_campaign_team)

    user = User.query.filter_by(email="staff@ftc-cyc.com").first()
    if user:
        pass
    else:
        db.session.add(user_staff)

    db.session.commit()


if __name__ == "__main__":
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://root:root-password@localhost/fates'
    app.run(debug=True, host="0.0.0.0")
