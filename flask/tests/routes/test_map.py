import sys
sys.path.append('flask/api/routes')

from map import scaleDataPointZ  # noqa: E402 - can't be imported at the top here


def test_scaleDataPointZ_min():
    point = 35200
    map_id = 3

    point_on_map = scaleDataPointZ(point, map_id)

    assert point_on_map == 0


def test_scaleDataPointZ_max():
    point = 450930
    map_id = 3

    point_on_map = scaleDataPointZ(point, map_id)

    assert point_on_map == 144.3
