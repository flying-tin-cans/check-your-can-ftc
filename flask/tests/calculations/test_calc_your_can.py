import pytest
import sys
sys.path.append('flask/api')

from calculations.calc_your_can import Calc_your_can  # noqa: E402 - can't be imported at the top here
from calculations.fate import Fate  # noqa: E402 - can't be imported at the top here


@pytest.mark.skip(reason="Needs secret.json, which is not available in the CI")
def test_executeForMission():
    calc_your_can = Calc_your_can("flask/configs/secret.json")
    fates, wrap_ups, frontline = calc_your_can.executeForMission(
        330, 328, "flask/tests/calculations/assets/Channel 1942 M5.Mission", "flask/configs/probabilities.json",
        "flask/maps")

    # Fate shall be calculated
    for fate in fates:
        if fate["fate"] == Fate.NOT_CALCULATED:
            assert False
        else:
            assert True
