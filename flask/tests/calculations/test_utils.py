import sys
sys.path.append('flask/api/calculations')

from utils import Utils  # noqa: E402 - can't be imported at the top here


def test_calculateEscape_woundMax():
    utils = Utils(False, False, False)

    escape = utils.calculateEscape(60, "flask/configs/probabilities.json", 100)
    if escape["Captured-en-route"] is True:
        # Pilot should be captured imminently every time his wounds are 100
        assert True
    else:
        assert False


def test_calculateAsr_woundMax():
    utils = Utils(False, False, False)

    escape = utils.calculateAsr(60, "flask/configs/probabilities.json", 100, 0)
    if escape["Drowned"] is True:
        # Pilot should be drowned imminently every time his wounds are 100
        assert True
    else:
        assert False


def test_calculateAsr_seaStateMax():
    utils = Utils(False, False, False)

    escape = utils.calculateAsr(120, "flask/configs/probabilities.json", 0, 9)
    if escape["Drowned"] is True:
        # Pilot should be drowned imminently every time the sea state is 9
        assert True
    else:
        assert False
