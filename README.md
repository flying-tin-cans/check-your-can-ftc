# Check-Your-Can: README

## Pipeline status

### Main
[![pipeline status](https://gitlab.com/flying-tin-cans/check-your-can-ftc/badges/main/pipeline.svg)](https://gitlab.com/flying-tin-cans/check-your-can-ftc/-/commits/main)

### Dev
[![pipeline status](https://gitlab.com/flying-tin-cans/check-your-can-ftc/badges/dev/pipeline.svg)](https://gitlab.com/flying-tin-cans/check-your-can-ftc/-/commits/dev)

## Overview

#### Components used for FTCs "Check-Your-Fate" feature:

TODO


## Set-Up

### Git-lab runner

See: https://docs.gitlab.com/runner/install/linux-repository.html
```
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
```

### Productive Environment

Run:
```
./build_and_run_container.sh
```

To make a script executable, use ```chmod +x filename.sh```.

### Dev-Environment

For creating the MySQL database, run
```
docker-compose up
```
You can modify the docker-compose.yml to just start up the fate-db.
Then go to ```cd flask/api ``` and run ```python app.py ```.
You should now be able to access the [Website](localhost:5000).

### Other helpfull stuff

#### Fix "-bash: /bin/sh^M: bad interpreter: No such file or directory"

```
vim filename
:set fileformat=unix
:wq
```

#### Stop all container, remove them and their images

    1. docker kill $(docker ps -q)
    2. docker rm $(docker ps -a -q)
    3. docker rmi $(docker images -q)
    4. docker volume rm $(docker volume ls -q)

#### Linux dev-environment
```
pip3 install virtualenv
virtualenv venv
source venv/bin/activate
pip3 install -r flask/configs/requirements.txt
deactivate
```

## Features

### Admin rights

TODO

### Campaign-Team rights

#### View map

View the map for an entire mission with all the events on it.

#### Mission Wrap-Up

WIP: Show the mission wrap-up with useful unit-performance informations

#### Manage missions

Manages the missions in the check-your-can database.

    - Upload \*.mission files
    - Add missions to check-your-can by selecting the PAM mission, the corresponding IL2-stats missions aswell as the uploaded mission

#### Process missions
    1. Calculates the fate and creates the maps for the added missions which are not yet processed.
    2. Deletes the \*.mission file to save space again.

#### Fates

Same as "Fates" in "Staff rights".

### Staff rights

#### Fates

Check the fates of every pilot for every processed mission.

#### Pilots-not-linked



### No special rights

#### Pilot fate for a mission
See the fate of a specific pilot in a specific mission via:
```
[Server-IP and port]/map_fate?pam_mission_id=[pam_mission_id]]&pam_pilot_id=[pam_pilot_id]
```

## Create the map tiles

Thankfully somebody create a docker container for this.

https://hub.docker.com/r/niene/gdal2tiles-leaflet

```
docker pull niene/gdal2tiles-leaflet
docker run -v `pwd`:/data niene/gdal2tiles-leaflet -l -p raster -z 0-7 -w none /data/<image> /data/<tilesdir>
```

## Over stuff

### gitreview-gpt

https://github.com/fynnfluegge/gitreview-gpt
