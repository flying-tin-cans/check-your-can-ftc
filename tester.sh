#!/bin/sh
pytest -v flask/tests/calculations/*.py --junitxml=flask/tests/test-reports/calculations_report.xml
pytest -v flask/tests/routes/*.py --junitxml=flask/tests/test-reports/routes_report.xml
